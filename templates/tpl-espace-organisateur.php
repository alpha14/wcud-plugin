<?php
/*
Template Name: Espace organisateur
*/

if(isset($_POST['password_org']) && (!empty($_POST['password_org']))):
    $user_connect = wp_get_current_user();
    wp_set_password($_POST['password_org'], $user_connect->ID);
    
    //Je log automatiquement l’utilisateur
    $creds = array();
    $creds['user_login']    = $user_connect->user_email;
    $creds['user_password'] = $_POST['password_org'];
    $creds['remember']      = true;
    $user = wp_signon( $creds, false );
endif;
?>

<?php get_header(); ?>

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
        <h2 class="big"><?php _e('Mon compte', 'cwcud');?></h2>
    </div>
</header>
        
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- if user connected -->
<?php if(!is_user_logged_in()):?>
<div class="form-container wrapper">
    <?php wp_login_form(
        array(
            'remember'       => false,
            'label_username' => __( 'Identifiant', 'cwcud' ),
            'label_password' => __( 'Mot de passe', 'cwcud' ),
            'remember'		 => true,
            'redirect'       => get_permalink(get_field("page_list_cleanup", "options")),
        )
    );?>
    <a class="lost-pswd" href="<?php wp_lostpassword_url(home_url())?>"><?php _e("Mot de passe oublié ?", "cwcud"); ?></a>
</div>
<!-- if user is'nt connected -->
<?php 
else:
?>

    <form action="" method="post" id="form_admin_organisateur" class="organizer-admin-infos form_admin_organisateur">
        <input type="hidden" name="honeyPot" value="">

        <?php

        // check whitch user is connect

        $user_connect = wp_get_current_user();

        $userGender = get_user_meta($user_connect->ID, 'user_gender', true );
        // var_dump($user_connect_meta)
        $userFirstName = get_user_meta($user_connect->ID, 'first_name', true );
        $userLastName = get_user_meta($user_connect->ID, 'last_name', true );
        $userPhone = get_user_meta($user_connect->ID, 'user_phone', true);
        $userMail = $user_connect->user_email;

        ?> 

        <!-- Firstname et lastname -->
        <?php if($userGender == 'M'):
            echo '<p>Mr '.$userFirstName.' '.$userLastName.'</p>';
        elseif($userGender == 'F'):
            echo '<p>Mr '.'Mme '.$userFirstName.' '.$userLastName.'</p>';
        endif;    
        ?>

        <!-- Phone -->
        <p class="form-sub-item"><?php echo '<span class="label-like">Téléphone :</span> '.$userPhone;?></p>

        <!-- Mail -->
        <p class="form-sub-item"><?php echo '<span class="label-like">Adresse mail : </span>'.$userMail;?></p>

        <!-- pwd -->
        <div id="password_org" class="form-sub-item form-row">
            <label class="no-margin space-right" for="password_org"><?php _e('Mot de passe : ', 'cwcud');?></label>
            <input type="password" name="password_org" id="password_org" value="<?php echo $userPwd; ?>" required>
        </div>
        <input type="submit" id="sendMessage" class="button form-item" value="<?php _e('Modifier le mot de passe', 'cwcud'); ?>" >
    </form>
    
<?php endif; ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php 
get_footer(); 
?>

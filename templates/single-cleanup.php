<?php get_header(); ?>



<!-- Title -->
<header  id="cleanup-header" class="wrapper">
	<h1 class="no-margin"><?php the_title();?></h1>
</header>

<main class="wrapper" id="cleanup-layout">

	<div id="cleanup-content" class="no-useless-margin">
	<?php
//désinscription particupant
if(isset($_GET['remove_email']) && $_GET['key'] == crypt(sanitize_email($_GET['remove_email']), 'windows95')){
	//delete participant
	global $wpdb;

	$db_item =  $wpdb->get_results($wpdb->prepare(
		"SELECT id FROM {$wpdb->prefix}participant WHERE email LIKE %s", sanitize_email($_GET['remove_email'])
	));
	if($wpdb->num_rows == 1){
		$id_participant = $db_item[0]->id;
		$wpdb->delete( $wpdb->prefix.'participant_cleanup', array( 'id_participant' => $id_participant, 'id_cleanup' => get_the_id() ) );

		echo "<h2>Votre désinscription à bien été prise en compte.</h2>";

		$db_item =  $wpdb->get_results($wpdb->prepare(
			"SELECT id_cleanup FROM {$wpdb->prefix}participant_cleanup WHERE id_participant = %d", $id_participant
		));

		if($wpdb->num_rows == 0){
			$wpdb->delete( $wpdb->prefix.'participant', array( 'id' => $id_participant ) );
			echo "<br><h2>Toutes les informations vous concernant, ont été supprimées.</h2>";
		}

	}	
}
?>
		<!-- Description -->
		<p>
			<span class="label-like"><?php _e('Description : ') ;?></span></br>
			<?php the_content();//echo nl2br(get_post_meta( $post->ID, "description", true ));?>
		</p>

		<p>
			<button id="display-map-cleanup" class="button">Afficher la carte</button>
		</p>
		<div id="single-map-cleanup"></div>
		<?php
		$author_id = $post->post_author;
		$user = get_userdata( $author_id );
		$tab_cleanup = array();
		$tab_cleanup[] = array(
			'name'		=> get_the_title(),
			'link'		=> get_permalink(),
			'coordonate'=> get_post_meta( $post->ID, "coordonate", true ),
			'etat'		=> get_post_meta( $post->ID, "private", true ),
			'organisator'=> $user->first_name." ".$user->last_name.' - '.get_post_meta($post->ID,'structure_name', true),
			'date' 		=> "le ".date_i18n('j/m', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta( $post->ID, "time_start", true ))),
		);
		?>
		<script>
			var tab_cleanup = <?php echo json_encode($tab_cleanup);?>;
		</script>
		<style type="text/css">
			#single-map-cleanup{ /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
				height:650px;
			}
		</style>
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
		<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
		<!--<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />-->
		
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
		<!--<script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js'></script>-->
			
		<!-- date  -->
		
	</div>

	<div id="cleanup-infos" class="no-useless-margin">
		<div class="buttons-container">

			<?php
/*$tab_arg = array(
	'remove_email' => sanitize_email("romain@ihaveagreen.fr"),
	'key'	=> crypt(sanitize_email("romain@ihaveagreen.fr"), 'windows95'),
);
echo esc_url( add_query_arg( $tab_arg, get_the_permalink($_POST['id_cleanup'])));*/
			?>

			<?php
			if(get_post_meta( $post->ID, "private", true ) == "no_private"):
				$participants = (int)get_post_meta( $post->ID, 'participants', true );
				$nb_participant_max = (int)get_post_meta( $post->ID, "nb_participant_max", true );
				if( !empty($nb_participant_max) && $participants >= $nb_participant_max): 
					echo '<p>Le CyberCleanUp est complet</p>';
				else:
					?>
					<button class="button inscription_cleanup" data-id="<?php echo get_the_ID();?>" 
						data-title="<h2>Inscription : <?php echo str_replace('"', '', get_the_title());?></h2><p>Organisé par <?php echo get_post_meta($post->ID, "structure_name", true);?>, le <?php echo date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")";?></p>">
						<?php _e('Participer', 'cwcud'); ?>
					</button>
					<?php
				endif;
				?>

				<button class="button-yellow" id="get_modale_orga" data-id-cleanup="<?php echo get_the_ID();?>">
					<?php _e("Contacter l'organisateur", 'cwcud'); ?>
				</button>
			<?php
			else:
				echo '<p><span class="label-like">Le CyberCleanUp est privé</span></p>';
			endif;
			?>
		</div>
		
		<p>
			<?php 
				echo "Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")<br>";
			?>
		</p>

		<?php


		/*$resultats = $wpdb->get_results("SELECT ID FROM {$wpdb->prefix}participant") ;
		var_dump($wpdb->num_rows);
		$wpdb->insert(
			$wpdb->prefix.'participant',
			array(
				'email' => "test@test.fr"
			),
			array(
				'%s'
			)
		);*/

		$departements = arrayDepartements();
		$departement = get_post_meta($post->ID, "cleanup_departement", true);
		echo '<p>'.$departements[$departement].' - '.$departement.'</p>';
		?>

		<!-- if register 'structure name', display it -->
		<p>
			<?php $nameStructure = get_post_meta( $post->ID, "structure_name", true );
			if(!empty($nameStructure)):
				echo '<b>Organisé par</b> '.$nameStructure;
			endif;
			?>
		</p>

		<p>
			<?php 
				echo '<b>Numéro du CyberCleanUp</b> : '.$post->ID;
			?>
		</p>

		<!-- nb participant -->
		<p>
			<?php 
			$participants = (int)get_post_meta( $post->ID, 'participants', true );
			$nb_participant = get_post_meta( $post->ID, "nb_participant_max", true );
			
			echo '<span class="label-like">Nombre de participant : </span>'.$participants;
			if(!empty($nb_participant)){
				echo ' / '.$nb_participant; 
			}
			?>  
		</p>

		<!-- child autorisation -->
		<?php $child = get_post_meta( $post->ID, "child", true );?>
		<?php if ($child == 'yes_child'):?>
			<p class="label-like">
				<?php _e('Accessible aux enfants', 'cwcud'); ?>
			</p>
		<?php endif;?>

		<?php 
		if(get_post_meta( $post->ID, "private", true ) == "no_private"):
			$whichLocation = get_post_meta( $post->ID, "location", true ) ;
			$adresse = get_post_meta($post->ID, "cleanup_adresse", true);
			$link = (!empty(get_post_meta($post->ID, "link_connection", true))) ? '<a href="'.get_post_meta($post->ID, "link_connection", true).'" target="_BLANK">'.get_post_meta($post->ID, "link_connection", true).'</a>' : 'Le lien vous sera envoyé ultérieurement par email.';
			$otherInformation = get_post_meta( $post->ID, "more_information", true );

			if ($whichLocation === "location_facetoface"):
				echo '<p><span class="label-like">Évènement présentiel :</span> '.$adresse.'<br>'.$otherInformation.'</p>';
			elseif($whichLocation === "location_distancing"):
				echo '<p><span class="label-like">Évènement distanciel : </span>'.$link.'</p>';
			elseif($whichLocation === "location_both"):
				echo '<p><span class="label-like">Évènement présentiel :</span> '.$adresse.' - '.$otherInformation;
				echo '<br><span class="label-like">Évènement distanciel :</span> '.$link.'</p>';
			endif;
		endif;
		?>

		<!-- share on social medias -->
		<div id="share">
			<p class="label-like"><?php esc_html_e('Partager :', 'cwcud')?></p>

			<a class="JSrslink link-icon" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Facebook', 'cwcud')?>">
				<img alt="<?php esc_html_e('Facebook', 'cwcud')?>" src="<?php echo get_template_directory_uri(); ?>/image/facebook.png" height="40" width="40">
			</a>

			<a class="JSrslink link-icon" href="https://www.twitter.com/share?url=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Twitter', 'cwcud')?>">
				<img alt="<?php esc_html_e('Twitter', 'cwcud')?>" src="<?php echo get_template_directory_uri(); ?>/image/twitter.png" height="40" width="40">
			</a>

			<a class="JSrslink link-icon" href="https://www.linkedin.com/cws/share?url=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Linkedin', 'cwcud')?>">
				<img alt="<?php esc_html_e('Linkedin', 'cwcud')?>" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.png" height="40" width="40">
			</a>          
		</div>
	</div>
	  
	<div class="modale" id="modale_inscription_cleanup">
		<p style="text-align:center;cursor:pointer;color:white" class="close-modale">X Fermer</p>
		<div class="cleanup-modale">
			<div id="modale_title_cleanup">
			</div>
			<form class="form-white" action="" method="post" name="registerUserCleanUp" id="registerUserCleanUp">
				<label for="name_registerUserCleanUp"><?php _e('Nom', 'cwcud') ?>*</label>
				<input type="text" name="name_registerUserCleanUp" id="name_registerUserCleanUp" placeholder="<?php _e('Véronique Dubois', 'cwcud'); ?>" required>

				<label for="phone_registerUserCleanUp"><?php _e('Téléphone', 'cwcud') ?></label>
				<input type="tel" name="phone_registerUserCleanUp" id="phone_registerUserCleanUp" placeholder="<?php _e('+33 6 01 02 03 04', 'cwcud'); ?>">

				<label for="email_registerUserCleanUp"><?php _e('Adresse e-mail', 'cwcud') ?>*</label>
				<input type="email" name="email_registerUserCleanUp" id="email_registerUserCleanUp" required placeholder="<?php _e('adresse@mail.com', 'cwcud'); ?>">

				<p class="form-info">
				<?php _e('* Votre adresse email sera utilisée pour vous recontacter et vous tenir infomé.', 'cwcud'); ?>
				</p>

				<?php if ($child == 'yes_child'):?>
				<label for="invite_registerUserCleanUp"><?php _e("Nombre d'adultes invités (en plus de vous)", 'cwcud') ?></label>
				<input type="number" min="0" name="invite_registerUserCleanUp" id="invite_registerUserCleanUp" value="" placeholder="Laisser vide si pas d'invité">
				
				<label for="invite_child_registerUserCleanUp"><?php _e("Nombre d'enfants invités", 'cwcud') ?></label>
				<input type="number" min="0" name="invite_child_registerUserCleanUp" id="invite_child_registerUserCleanUp"  value="" placeholder="Laisser vide si pas d'invité">
				<?php else:?>
				<label for="invite_registerUserCleanUp"><?php _e("Nombre d'invités (en plus de vous)", 'cwcud') ?></label>
				<input type="number" min="0" name="invite_registerUserCleanUp" id="invite_registerUserCleanUp"  value="" placeholder="Laisser vide si pas d'invité">
				
				<input type="hidden" name="invite_child_registerUserCleanUp" id="invite_child_registerUserCleanUp" >
				<?php endif;?>
				<p class="form-item no-margin form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

				<div class="checkbox form-item">
					<input type="checkbox" name="ok_cgu" id="ok_cgu" required>
					<div class="checkbox-label">
						<label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Cyber World CleanUp Day.', 'cwcud');?>*</label>
						<div class="form-info">
							<a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a>
						</div>
					</div>
				</div>

				<div class="checkbox form-item">
					<input type="checkbox" name="checkbox_newsletter" id="checkbox_newsletter" value="true">
					<div class="checkbox-label">
						<label for="checkbox_newsletter">
							<?php _e("Je désire m'inscrire à la newsletter du CyberWorlCleanUpDay", 'cwcud');?>
						</label>
					</div>
				</div>

				<input type="hidden" name="honeypot" value="">
				<input type="hidden" name="id_cleanup" id="id_cleanup" value="">
				<input type="submit" class="button form-item" id="sendRegisterUserCleanUp" value="<?php _e("Valider l'inscription", 'cwcud'); ?>">
				<div id="ResponseMessageRegisterUserCleanUp" class="ResponseMessageRegisterUserCleanUp">
					<p class="ctr"><?php _e('Merci, votre inscription a été enregistrée.', '');?></p>
				</div>
			</form>
		</div>
	</div>

	<div class="modale" id="modale_orga">
		<p style="text-align:center;cursor:pointer;color:white" class="close-modale">X Fermer</p>
		<div class="embassy-modale" id="modale_orga_content">
		</div>
	</div>
	
</main>

<?php get_footer(); ?>

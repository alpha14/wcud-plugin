<?php
/*
Template Name: Listing Cleanup Organisateur
*/
$current_user = wp_get_current_user();
if ( 0 == $current_user->ID || ( !in_array( 'organisateur', $current_user->roles, true ) )) :
    wp_redirect( home_url(), 302);
else:
?>

<?php get_header(); ?>

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Mes CyberCleanUps', 'cwcud');?></h2>
		<a class="button" href="<?php echo get_permalink( get_field("page_add_cleanup", "option"));?>"><?php _e('Créer un nouveau CyberCleanUp', 'cwcud'); ?></a>
    </div>
</header>


<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<!-- Listing Archive -->
<section class="wrapper">
	
	<?php
	global $current_user;
	get_currentuserinfo();
	$arg = array(
		'posts_per_page'    => -1,
		'post_status' 		=> array('publish', 'private'),
		'post_type'			=> 'cleanup',
		'author'        =>  $current_user->ID,
	);

	$posts = get_posts( $arg );

	// <!-- pour le scroll -->
	//$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);

	if($posts):
	?>

		<div class="listing-cleanup-organizer-view">
		
			<?php 
			$url_bilan = get_field("url_bilan", "option");
			foreach($posts as $post): 		
				setup_postdata( $post );
				?>
				<article class="cleanup-card organizer-view">
					<div class="info-container no-useless-margin">
						<!-- Title -->
						<h3>
						<?php
						$is_private = get_post_meta( $post->ID, "private", true );
						if ( $is_private == "no_private" ) {
							echo '<img src="'.plugin_dir_url( __DIR__ ) . 'assets/picto_public.png" width="20"> '; 
							the_title();			
						}
						else{
							echo '<img src="'.plugin_dir_url( __DIR__ ) . 'assets/picto_private.png"  width="20"> '; 
							the_title();
						}
						?>
						</h3>
						<!-- date  -->
						<p>
							<?php 
							echo "Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")<br>";
							?>
						</p>
					
						<!-- nb max -->
						<?php 
						$participants = (int)get_post_meta( $post->ID, 'participants', true );
						$nb_participant_max = get_post_meta( $post->ID, "nb_participant_max", true );
						echo '<p>';
							echo '<span class="label-like"> Nombre d\'inscrits :</span> ';
							echo $participants;
							if(!empty($nb_participant_max)):
							echo ' / '.$nb_participant_max;
							endif;
						echo '</p>';
					
						?>
						
						<!-- Email -->
						<?php 
						$user_email = get_post_meta( $post->ID, "user_email", true );
						if(!empty($user_email)):
							echo '<p><span class="label-like">Adresse e-mail :</span> '.$user_email.'</p>';
						endif;
						?>

						<!-- Phone -->
						<?php 
						$user_phone = get_post_meta( $post->ID, "user_phone", true );
						if(!empty($user_phone)):
							echo '<p><span class="label-like">Téléphone :</span> '.$user_phone.'</p>';
						endif;
						?>

						<?php 
						echo '<p><span class="label-like">ID CyberCleanUp :</span> '.get_the_ID().'</p>';
						?>
					</div>
					<div class="btn-container no-useless-margin">
						<a class="button" href="<?php the_permalink();?>" target="_blank">
							<?php _e('Visualiser', 'cwcud');?> 
						</a>

						<a class="button" href="<?php echo esc_url( add_query_arg( 'id_cleanup', $post->ID, get_permalink( get_field("page_update_cleanup", "option") ) ) );?>">
							<?php _e('Modifier', 'cwcud');?>
						</a>

						<?php if($participants > 0):?>
						<a class="button get_list_inscrits" href="#" data-id="<?php echo $post->ID;?>">
							<?php _e('Voir les inscrits', 'cwcud');?>
						</a>
						<?php endif;?>

						<?php if(!empty($url_bilan)):?>
						<a class="button" href="<?php echo $url_bilan.$post->ID;?>" target="_blank">
							<?php _e('Formulaire bilan', 'cwcud');?>
						</a>
						<?php endif;?>
					</div>
				</article>
			<?php
			endforeach; 
			wp_reset_postdata(); 
			?>
		</div><!-- /listing-cleanup-organizer-view -->
		
	<div class="modale" id="modal_list_inscrits">
		<p style="text-align:center;cursor:pointer;color:white" class="close-modale">X Fermer</p>
		<div class="embassy-modale" id="list_inscrits">
		</div>
	</div>
		
	
	<?php
	else :

		//get_template_part( 'template-parts/content', 'none' );

	endif;
	?>

</section><!-- End of Listing Archive -->

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); 

endif;
?>

<?php
/*
Template Name: Nouveau CleanUp
*/
$current_user = wp_get_current_user();
if ( 0 == $current_user->ID || ( !in_array( 'organisateur', $current_user->roles, true ) )) :
    wp_redirect( home_url(), 302);
else:
?>

<?php get_header(); ?> 

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Organiser un Cyber CleanUp', 'cwcud');?></h2>
    </div>
</header>
        
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<section class="wrapper">
    <form action="" method="post"  name="cleanupForm" id="cleanupForm" class="form-style">
        <input type="hidden" name="honeyPot" value="">

        <h2 class="ctr no-margin"><?php _e('Organiser un Cyber CleanUp', 'cwcud');?></h2>
        
        <div class="form-item">
            <p class="no-margin label-like"><?php _e('Type de structure *', 'cwcud');?></p>

            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="citizen" class="structure" value="citizen" onclick="displayRadioValue()" checked>
                <label class="checkbox-label" for="citizen"><?php _e('Citoyen', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="association" class="structure" value="association" onclick="displayRadioValue()">
                <label class="checkbox-label" for="association"><?php _e('Association', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="school" class="structure" value="school" onclick="displayRadioValue()">
                <label class="checkbox-label" for="school"><?php _e('École', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="collectivity" class="structure" value="collectivity" onclick="displayRadioValue()">
                <label class="checkbox-label" for="collectivity"><?php _e('Collectivité', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="company" class="structure" value="company" onclick="displayRadioValue()">
                <label class="checkbox-label" for="company"><?php _e('Entreprise', 'cwcud');?></label>
            </div>
            <!--<div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="sponsorship" class="structure" value="sponsorship" onclick="displayRadioValue()">
                <label class="checkbox-label" for="sponsorship"><?php _e('Parainage', 'cwcud');?></label>
            </div>-->
        </div>

        <div class="form-item" id="structure_name" style="display:none">
            <label for="structure_name_id"><?php _e('Nom de la structure', 'cwcud'); ?></label>
            <input type="text"  id="structure_name_id" name="structure_name" placeholder="Nom de votre structure" >
        </div>

        <label for="cleanup_name"><?php _e('Nom du CyberCleanUp *', 'cwcud'); ?></label>
        <input type="text" id="cleanup_name" name="cleanup_name" placeholder="<?php _e('Nom du CleanUp', 'cwcud'); ?>"  required>

        <label for="description"><?php _e('Description *', 'cwcud'); ?></label>
        <textarea id="description" name="description" rows="7" placeholder="<?php _e('Préciser le lieu, le déroulement, la durée du CyberCleanUp..', 'cwcud'); ?>"  required></textarea>

        <label for="date_start"><?php _e('Début du CyberCleanUp *', 'cwcud'); ?></label>
        <input type="date" id="date_start" name="date_start" min="<?php the_field("date_start_event", "option");?>" max="<?php the_field("date_end_event", "option");?>" required>

        <label for="time_start"><?php _e('Heure de début du CyberCleanUp *', 'cwcud'); ?></label>
        <input type="time" id="time_start" step="" name="time_start" required>
      
        <div class="form-item" >
            <label for="time_end"><?php _e('Durée du CyberCleanUp :', 'cwcud'); ?></label>
            <div class="select">
                <select name="time_end" id="time_end" required>
                    <?php
                    $t = get_during();
                    foreach ($t as $value):?>
                        <option value="<?php echo $value;?>" ><?php echo $value;?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

        <label for="cleanup_tag"><?php _e('Tag du CyberCleanUp', 'cwcud'); ?></label>
        <input type="text" id="cleanup_tag" name="cleanup_tag" placeholder="<?php _e('Tag du CyberCleanUp', 'cwcud'); ?>">

        <p class="form-item no-margin label-like"><?php _e('Afficher le CyberCleanUp sur le site*', 'cwcud');?></p>
        <div class="form-row">
            <div class="checkbox">
                <input type="radio" name="onMap" id="yes_on_map" value="yes_on_map" onclick="displayMap()" checked>
                <label class="checkbox-label" for="yes_on_map"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="checkbox">
                <input type="radio" name="onMap" id="no_on_map"  value="no_on_map" onclick="displayMap()">
                <label class="checkbox-label" for="no_on_map"><?php _e('Non', 'cwcud');?></label>
            </div>
        </div>
        <p class="form-info form-sub-item"><?php _e("Si le CyberCleanUp n’est pas affiché, il ne sera pas visible sur la carte et sur l’annuaire, les inscriptions seront fermées et vos coordonnées masquées", 'cwcud');?></p>
        
        <p class="form-item no-margin label-like"><?php _e('Le CyberCleanUp est-il privé ? *', 'cwcud');?></p>
        <div class="form-row">
            <div class="checkbox">
                <input type="radio" name="private" id="yes_private" value="yes_private" >
                <label class="checkbox-label" for="yes_private"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="checkbox">
                <input type="radio" name="private" id="no_private"  value="no_private" checked>
                <label class="checkbox-label" for="no_private"><?php _e('Non', 'cwcud');?></label>
            </div>
        </div>
        <p class="form-info form-sub-item"><?php _e("Si le CyberCleanUp est privé, les inscriptions seront fermées, vos coordonnées masquées et le lieu de rendez-vous et/ou le lien de visioconférence seront masqués", 'cwcud');?></p>

        <p class="form-item no-margin label-like"><?php _e('Le CyberCleanUp est-il accessible aux enfants ? *', 'cwcud');?></p>
        <div class="form-row">
            <div class="checkbox">
                <input type="radio" name="child" id="yes_child" class="child" value="yes_child" checked>
                <label class="checkbox-label" for="yes_child"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="checkbox">
                <input type="radio" name="child" id="no_child" class="child" value="no_child">
                <label class="checkbox-label" for="no_child"><?php _e('Non', 'cwcud');?></label>
            </div>
        </div>

        <label for="nb_participant_max"><?php _e('Nombre maximum de participants :', 'cwcud'); ?></label>
        <input type="number" min="1" id="nb_participant_max" name="nb_participant_max" placeholder="<?php _e('Laisser vide si Illimité', 'cwcud'); ?>"  >
        <p class="form-info form-sub-item"><?php _e('Laissez le champ vide pour un nombre illimité de participants.', 'cwcud');?></p>
        
        <label for="user_phone"><?php _e('Téléphone', 'cwcud'); ?>*</label>
        <input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04"  required>
        <p class="form-info form-sub-item"><?php _e('Cette information sera visible sur le site Cyber World CleanUp Day uniquement si votre CyberCleanUp est public - elle permettra aux participants de vous contacter.', 'cwcud');?></p>

        <label for="user_email"><?php _e('Adresse email', 'cwcud'); ?>*</label>
        <input type="email" id="user_email" name="user_email" placeholder="adresse.mail@exemple.com" required>
        <p class="form-info form-sub-item"><?php _e('Cette information sera visible sur le site Cyber World CleanUp Day uniquement si votre CyberCleanUp est public - elle permettra aux participants de vous contacter.', 'cwcud');?></p>

        <!--<p class="form-alert form-item"><?php _e('<strong>Attention :</strong> vous n\'avez aucun moyen de contact : les participants ne pourront pas se renseigner avant de s\'inscrire au CyberCleanUp. Veuillez renseigner au minimum <strong>un numéro de téléphone</strong> ou une <strong> adresse mail</strong>', 'cwcud') ;?></p>-->

        <p class="form-item no-margin label-like"><?php _e('Le CyberCleanUp est-il distanciel ou présentiel ? *', 'cwcud');?></p>
        <div class="checkbox form-sub-item">    
            <input type="radio" name="location" id="location_facetoface" class="location" value="location_facetoface" onclick="displayInformations()" checked>
            <label class="checkbox-label" for="location_facetoface"><?php _e('Présenciel', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">   
            <input type="radio" name="location" id="location_distancing" class="location" value="location_distancing" onclick="displayInformations()" >
            <label class="checkbox-label" for="location_distancing"><?php _e('Distanciel', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">   
            <input type="radio" name="location" id="location_both" class="location" value="location_both" onclick="displayInformations()" >
            <label class="checkbox-label" for="location_both"><?php _e('Présentiel et distanciel', 'cwcud');?></label>
        </div>

        <div class="form-item" id="cleanup_adresse">
            <label for="cleanup_adresse_id"><?php _e('Adresse du CyberCleanUp :', 'cwcud'); ?>*</label>
            <input type="text" name="cleanup_adresse" id="cleanup_adresse_id" placeholder="<?php _e('4 boulevard de Lafayette, 76100 Rouen', 'cwcud'); ?>" required>
        </div>

        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
        <style type="text/css">
            #map{ /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
                height:400px;
            }
        </style>
        <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
        
        <div class="form-item" id="allMap">
            <!--<label for="addressMap" required><?php _e('Placer le CyberCleanUp sur la carte :', 'cwcud') ;?></label>-->
            <a href="#" class="button" id="searchAdressMap">Positionner le picto à partir l'adresse renseignée</a>
            <div class="form-sub-item" id="map"></div>    
            <input class="form-sub-item" type="hidden" name="coordonate" id="coordonate">
        </div>
        
        

        <div class="form-item" >
            <label for="cleanup_departement"><?php _e('Département du CyberCleanUp :', 'cwcud'); ?>*</label>
            <div class="select">
                <select name="cleanup_departement" id="cleanup_departement" required>
                    <option value="">Selectionnez votre départements</option>
                    <?php
                    $tab_departements = arrayDepartements();
                    foreach ($tab_departements as $key => $value):?>
                        <option value="<?php echo $key;?>" ><?php echo $key;?> - <?php echo $value;?> </option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

        <div class="form-item" id="more_information">
            <label for="more_information"><?php _e('Informations complémentaires :', 'cwcud'); ?></label>
            <input type="text" name="more_information" placeholder="<?php _e('Salle de conférence au 2e étage', 'cwcud'); ?>">
        </div>

        <div class="form-item" id="link_connection" style="display:none">
            <label for="link_connection"><?php _e('Lien de connexion :', 'cwcud'); ?></label>
            <input type="url" name="link_connection" placeholder="<?php _e('https://www.visioconference.com', 'cwcud'); ?>">
            <p class="form-info form-sub-item"><?php _e('Si vous ne connaissez le lien, vous pourrez le renseigner dans un second temps', 'cwcud');?></p>
        </div>

        <p class="form-item form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

        <!-- Button submit -->
        <button class="form-item  button" type="submit" id="sendMessage"><?php _e('Créer le CyberCleanUp', 'cwcud'); ?></button>

    </form>
</section>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php 
get_footer(); 
endif;
?>

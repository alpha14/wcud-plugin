<?php
/*
Template Name: Update CleanUp
*/
$current_user = wp_get_current_user();

if ( 0 == $current_user->ID || !(in_array( 'organisateur', $current_user->roles, true ) || in_array( 'administrator', $current_user->roles, true ) ) ) :
    //wp_redirect( home_url(), 302);
    var_dump( (0 == $current_user->ID)  );
    var_dump(  (in_array( 'organisateur', $current_user->roles, true )) );
    var_dump(  (in_array( 'administrator', $current_user->roles, true )) );
else:
    if(empty($_GET['id_cleanup'])){
        wp_redirect( home_url(), 302);
    }

?>

<?php get_header(); ?>

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Organiser un Cyber CleanUp', 'cwcud');?></h2>
    </div>
</header>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<?php
$post = get_post($_GET['id_cleanup']);
setup_postdata($post);
?>
<section class="wrapper">
    <form action="" method="post"  name="updateCleanupForm" id="updateCleanupForm"  class="form-style">
        <input type="hidden" name="honeyPot" value="">
        <input type="hidden" name="id" value="<?php echo get_the_id();?>">
        <?php
        if(isset($_GET['imadmin'])){
            echo '<script>var action_url = "'.admin_url().'edit.php?post_type=cleanup";</script>';
        }
        ?>
        <h2 class="ctr no-margin"><?php _e('Modifier un Cyber CleanUp', 'cwcud');?></h2>
        
        <div class="form-item">
            <p class="no-margin label-like"><?php _e('Type de structure *', 'cwcud');?></p>
            <?php $whichStructureChecked = get_post_meta( $post->ID, "type_structure", true ) ;
            //var_dump($whichStructureChecked);?>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="citizen" class="structure" value="citizen" onclick="displayRadioValue()" <?php if($whichStructureChecked == 'citizen'): echo 'checked'; endif; ?> >
                <label class="checkbox-label" for="citizen"><?php _e('Citoyen', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="association" class="structure" value="association" onclick="displayRadioValue()" <?php if($whichStructureChecked == 'association'): echo 'checked'; endif; ?>>
                <label class="checkbox-label" for="association"><?php _e('Association', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="school" class="structure" value="school" onclick="displayRadioValue()" <?php if($whichStructureChecked == 'school'): echo 'checked'; endif; ?>>
                <label class="checkbox-label" for="school"><?php _e('École', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="collectivity" class="structure" value="collectivity" onclick="displayRadioValue()" <?php if($whichStructureChecked == 'collectivity'): echo 'checked'; endif; ?>>
                <label class="checkbox-label" for="collectivity"><?php _e('Collectivité', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="company" class="structure" value="company" onclick="displayRadioValue()" <?php if($whichStructureChecked == 'company'): echo 'checked'; endif; ?>>
                <label class="checkbox-label" for="company"><?php _e('Entreprise', 'cwcud');?></label>
            </div>
            <!--<div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="sponsorship" class="structure" value="sponsorship" onclick="displayRadioValue()" <?php if($whichStructureChecked == 'sponsorship'): echo 'checked'; endif; ?> >
                <label class="checkbox-label" for="sponsorship"><?php _e('Parainage', 'cwcud');?></label>
            </div>-->
        </div>

        <div class="form-item" id="structure_name" style="display:none">
            <label for="structure_name_id"><?php _e('Nom de la structure', 'cwcud'); ?></label>
            <input type="text"  name="structure_name" id="structure_name_id" placeholder="Nom de votre structure" value="<?php echo get_post_meta( $post->ID, "structure_name", true );?>">
        </div>

        <label for="cleanup_name"><?php _e('Nom du CleanUp *', 'cwcud'); ?></label>
        <input type="text" id="cleanup_name" name="cleanup_name" placeholder="<?php _e('Nom du CleanUp', 'cwcud'); ?>" value="<?php the_title();?>">

        <label for="description"><?php _e('Description *', 'cwcud'); ?></label>
        <textarea id="description" name="description" rows="7" placeholder="<?php _e('Préciser le lieu, le déroulement, la durée du CyberCleanUp..', 'cwcud'); ?>"  required><?php echo get_the_content();?></textarea>
 
        <label for="date_start"><?php _e('Début du CyberCleanUp *', 'cwcud'); ?></label>
        <input type="date" id="date_start" name="date_start" min="<?php the_field("date_start_event", "option");?>" max="<?php the_field("date_end_event", "option");?>"  value="<?php echo get_post_meta( $post->ID, "date_start", true );?>" required>

        <label for="time_start"><?php _e('Heure de début du CyberCleanUp *', 'cwcud'); ?></label>
        <input type="time" id="time_start" name="time_start" step="30"  value="<?php echo get_post_meta( $post->ID, "time_start", true );?>" required>
      
        <div class="form-item" >
            <label for="time_end"><?php _e('Durée du CyberCleanUp :', 'cwcud'); ?></label>
            <div class="select">
                <select name="time_end" id="time_end" required>
                    <?php
                    $t = get_during();
                    foreach ($t as $value):?>
                        <option value="<?php echo $value;?>" <?php selected( $value, get_post_meta( $post->ID, "time_end", true ) ); ?>><?php echo $value;?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

        <label for="cleanup_tag"><?php _e('Tag du CyberCleanUp', 'cwcud'); ?></label>
        <input type="text" id="cleanup_tag" name="cleanup_tag" placeholder="<?php _e('Tag du CyberCleanUp', 'cwcud'); ?>" value="<?php echo get_post_meta( $post->ID, "cleanup_tag", true );?>">

        <?php $isOnMap = get_post_meta( $post->ID, "onMap", true ); ?>
        <p class="form-item no-margin label-like"><?php _e('Afficher le CyberCleanUp sur le site*', 'cwcud');?></p>
        <div class="form-row">
            <div class="checkbox">
                <input type="radio" name="onMap" id="yes_on_map" value="yes_on_map" onclick="displayMap()" <?php if ($isOnMap == 'yes_on_map'): echo 'checked'; endif; ?> >
                <label class="checkbox-label" for="yes_on_map"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="checkbox">
                <input type="radio" name="onMap" id="no_on_map" value="no_on_map" onclick="displayMap()" <?php if ($isOnMap == 'no_on_map'): echo 'checked'; endif; ?>>
                <label class="checkbox-label" for="no_on_map"><?php _e('Non', 'cwcud');?></label>
            </div>
        </div>
        <p class="form-info form-sub-item"><?php _e("Si le CyberCleanUp n’est pas affiché, il ne sera pas visible sur la carte et sur l’annuaire, les inscriptions seront fermées et vos coordonnées masquées", 'cwcud');?></p>
        
        <p class="form-item no-margin label-like"><?php _e('Le CyberCleanUp est-il privé ? *', 'cwcud');?></p>
        <div class="form-row">
            <?php $isPrivate = get_post_meta( $post->ID, "private", true );?>
            <div class="checkbox">
                <input type="radio" name="private" id="yes_private" class="private" value="yes_private" <?php if ($isPrivate == 'yes_private'): echo 'checked'; endif; ?> >
                <label class="checkbox-label" for="yes_private"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="checkbox">
                <input type="radio" name="private" id="no_private" class="private" value="no_private" <?php if ($isPrivate == 'no_private'): echo 'checked'; endif; ?>>
                <label class="checkbox-label" for="no_private"><?php _e('Non', 'cwcud');?></label>
            </div>
        </div>
        <p class="form-info form-sub-item"><?php _e("Si le CyberCleanUp est privé, les inscriptions seront fermées, vos coordonnées masquées et le lieu de rendez-vous et/ou le lien de visioconférence seront masqués", 'cwcud');?></p>

        <p class="form-item no-margin label-like"><?php _e('Le CyberCleanUp est-il accessible aux enfants ? *', 'cwcud');?></p>
        <?php $isChild = get_post_meta( $post->ID, "child", true );?>
        <div class="form-row">
            <div class="checkbox">
                <input type="radio" name="child" id="yes_child" class="child" value="yes_child" <?php if ($isChild == 'yes_child'): echo 'checked'; endif; ?> >
                <label class="checkbox-label" for="yes_child"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="checkbox">
                <input type="radio" name="child" id="no_child" class="child" value="no_child" <?php if ($isChild == 'no_child'): echo 'checked'; endif; ?>>
                <label class="checkbox-label" for="no_child"><?php _e('Non', 'cwcud');?></label>
            </div>
        </div>

        <label for="nb_participant_max"><?php _e('Nombre maximum de participants:', 'cwcud'); ?></label>
        <input type="number" id="nb_participant_max" name="nb_participant_max" placeholder="<?php _e('Laisser vide si Illimité', 'cwcud'); ?>" value="<?php echo get_post_meta( $post->ID, "nb_participant_max", true );?>">
        <p class="form-info form-sub-item"><?php _e('Laissez le champ vide pour un nombre illimité de participants.', 'cwcud');?></p>


        <label for="user_phone"><?php _e('Téléphone (contact pour les participants)', 'cwcud'); ?>*</label>
        <input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04" value="<?php echo get_post_meta( $post->ID, "user_phone", true );?>" required>
        <p class="form-info form-sub-item"><?php _e('Cette information sera visible sur le site Cyber World CleanUp Day uniquement si votre CyberCleanUp est public - elle permettra aux participants de vous contacter.', 'cwcud');?></p>

        <div>
            <label for="user_email"><?php _e('Adresse mail (pour les participants)', 'cwcud'); ?>*</label>
            <input type="email" id="user_email" name="user_email" placeholder="adresse.mail@exemple.com" value="<?php echo get_post_meta( $post->ID, "user_email", true );?>" required>
            <p class="form-info form-sub-item"><?php _e('Cette information sera visible sur le site Cyber World CleanUp Day uniquement si votre CyberCleanUp est public - elle permettra aux participants de vous contacter.', 'cwcud');?></p>
        </div>

        <!--<p>
            <?php _e('<strong>Attention :</strong> vous n\'avez aucun moyen de contact : les participants ne pourront pas se renseigner avant de s\'inscrire au CyberCleanUp. Veuillez renseigner au minimum <strong>un numéro de téléphone</strong> ou une <strong> adresse mail</strong>', 'cwcud') ;?>
        </p>-->

        <p class="form-item no-margin label-like"><?php _e('Le CyberCleanUp est-il distanciel ou présentiel ? *', 'cwcud');?></p>
        <div class="checkbox form-sub-item">    
            <?php $whichLocation = get_post_meta( $post->ID, "location", true ) ;?>
            <input type="radio" name="location" id="location_facetoface" class="location" value="location_facetoface" onclick="displayInformations()" <?php if ($whichLocation == 'location_facetoface'): echo 'checked'; endif; ?> >
            <label class="checkbox-label" for="location_facetoface"><?php _e('Présenciel', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="location" id="location_distancing" class="location" value="location_distancing" onclick="displayInformations()" <?php if ($whichLocation == 'location_distancing'): echo 'checked'; endif; ?>>
            <label class="checkbox-label" for="location_distancing"><?php _e('Distanciel', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="location" id="location_both" class="location" value="location_both" onclick="displayInformations()" <?php if ($whichLocation == 'location_both'): echo 'checked'; endif; ?>>
            <label class="checkbox-label" for="location_both"><?php _e('Présentiel et distanciel', 'cwcud');?></label>
        </div>

        <div id="cleanup_adresse">
            <label for="cleanup_adresse_id"><?php _e('Adresse du CyberCleanUp :', 'cwcud'); ?></label>
            <input type="text" name="cleanup_adresse" id="cleanup_adresse_id" placeholder="<?php _e('4 boulevard de Lafayette, 76100 Rouen', 'cwcud');?>" value="<?php echo get_post_meta( $post->ID, "cleanup_adresse", true );?>" >
        </div>

        <?php if (1 || $isOnMap == 'yes_on_map'):?> 
            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
            <style type="text/css">
                #map{ /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
                    height:400px;
                }
            </style>
            <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
            <div class="form-item" id="allMap">
                <!--<label for="addressMap" required><?php _e('Placer le CyberCleanUp sur la carte :', 'cwcud') ;?></label>-->
                <a href="#" class="button" id="searchAdressMap">Positionner le picto à partir l'adresse renseignée</a>
                <div class="form-sub-item" id="map"></div>    
                <input class="form-sub-item" type="hidden" name="coordonate" id="coordonate" value="<?php echo get_post_meta( $post->ID, "coordonate", true );?>">
            </div>
        <?php endif; ?>
        
        
        <div class="form-item">
            <label for="cleanup_departement"><?php _e('Département du CyberCleanUp :', 'cwcud'); ?>*</label>
            <div class="select">
                <select name="cleanup_departement" id="cleanup_departement">
                    <?php
                    $tab_departements = arrayDepartements();
                    foreach ($tab_departements as $key => $value):?>
                        <option value="<?php echo $key;?>" <?php selected( $key, get_post_meta( $post->ID, "cleanup_departement", true ) ); ?>><?php echo $key;?> - <?php echo $value;?> </option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>

        <div class="form-item" id="more_information">
            <label for="more_information"><?php _e('Informations complémentaires :', 'cwcud'); ?></label>
            <input type="text" name="more_information" placeholder="<?php _e('Salle de conférence au 2e étage', 'cwcud');?>" value="<?php echo get_post_meta( $post->ID, "more_information", true );?>">
        </div>

        <div class="form-item" id="link_connection" style="display:none;">
            <label for="link_connection"><?php _e('Lien de connexion :', 'cwcud'); ?></label>
            <input type="url" name="link_connection" placeholder="<?php _e('https://www.visioconference.com', 'cwcud');?>" value="<?php echo get_post_meta( $post->ID, "link_connection", true );?>">
        </div>
        <script>
        document.addEventListener('DOMContentLoaded', function() {
            displayInformations();
        });
        </script>

        <p class="form-item form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

        <!-- Button submit -->
        <button  class="form-item  button" type="submit" id="sendMessage"><?php _e('Mettre à jour le CyberCleanUp', 'cwcud'); ?></button>


    </form>
    <?php wp_reset_postdata();?>
    <!-- End of the loop -->
    <?php //endwhile; endif;?>
</section>
<?php 
get_footer(); 
endif;
?>

<?php
/*
Template Name: Inscription Organisateur
*/
?>

<?php get_header(); ?>

<!-- Header --> 
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Devenir organisateur', 'cwcud');?></h2>
    </div>
</header>



        
<!-- Begining of the loop -->
<?php //if (have_posts()) : while (have_posts()) : the_post(); ?>

<section class="wrapper">

    <form action="" method="post"  name="organisateurForm" id="organisateurForm" class="form-style">
        <input type="hidden" name="honeyPot" value="">
    
        <div class="form-item">
            <p class="no-margin label-like"><?php _e('Civilité *', 'cwcud');?></p>
            <div class="checkbox form-sub-item">
                <input type="radio" name="user_gender" id="user_female" value="F">
                <label class="checkbox-label" for="user_female"><?php _e('Madame', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="user_gender" id="user_male" value="M">
                <label class="checkbox-label" for="user_male"><?php _e('Monsieur', 'cwcud');?></label>
            </div>
        </div>

        <label for="user_lastname"><?php _e('Nom *', 'cwcud'); ?></label>
        <input type="text" id="user_lastname" name="user_lastname" placeholder="Dubois" required>

        <label for="user_firstname"><?php _e('Prénom *', 'cwcud'); ?></label>
        <input type="text" id="user_firstname" name="user_firstname" placeholder="Véronique" required>

        <label for="user_email"><?php _e('Adresse mail *', 'cwcud'); ?></label>
        <input type="email" id="user_email" name="user_email" placeholder="adresse.mail@exemple.com" required>

        <p class="form-info form-sub-item"><?php _e('* Votre adresse mail servira d\'identifiant pour votre compte Organisateur.', 'cwcud');?></p>


        <label for="user_phone"><?php _e('Téléphone *', 'cwcud'); ?></label>
        <input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04"  required>

        <div class="form-item">
            <p class="no-margin label-like"><?php _e('Je suis adhérent à l\'association <strong>World CleanUp Day France</strong> (pour la période de janvier à décembre 2021)* :', 'cwcud');?></p>
            <div class="checkbox form-sub-item">
                <input type="radio" name="adherent_cwcud" id="yes_adherent_cwcud" value="yes">
                <label class="checkbox-label" for="yes_adherent_cwcud"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="adherent_cwcud" id="no_adherent_cwcud" value="no">
                <label class="checkbox-label" for="no_adherent_cwcud"><?php _e('Non', 'cwcud');?></label>
            </div>
        </div>

        <div class="form-item">
            <p class="no-margin label-like"><?php _e('Je suis adhérent à l\'association <strong>Institut du Numérique Responsable</strong> (pour la période de janvier à décembre 2021)* :', 'cwcud');?></p>
            <div class="checkbox form-sub-item">
                <input type="radio" name="adherent_inr" id="yes_adherent_inr" value="yes">
                <label class="checkbox-label" for="yes_adherent_inr"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="adherent_inr" id="no_adherent_inr" value="no">
                <label class="checkbox-label" for="no_adherent_inr"><?php _e('Non', 'cwcud');?></label>
            </div>
        </div>

        <p class="form-item form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

        <p class="form-item no-margin label-like"><?php _e('Je certifie que : ', 'cwcud');?></p>

        <div class="checkbox form-item">
            <input type="checkbox" name="ok_responsable" id="ok_responsable" required>
            <div class="checkbox-label">
                <label for="ok_responsable"><?php _e('J\'ai conscience que je suis responsable du(des) Cyber World CleanUp(s) que j\'organise.', 'cwcud');?>*</label>
            </div>
        </div>

        <div class="checkbox form-item">
            <input type="checkbox" name="ok_cgu" id="ok_cgu" required>
            <div class="checkbox-label">
                <label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Cyber World CleanUp Day.', 'cwcud');?>*</label>
                <div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
            </div>
        </div>

        <button class="button form-item"  type="submit" id="sendMessage"><?php _e('Enregistrer', 'cwcud'); ?></button>

    </form>
</section>
<!-- End of the loop -->
<?php //endwhile; endif;?>

<?php get_footer(); ?>

<?php
/*
Template Name: Listing Cleanup
*/
?>

<?php get_header(); ?>

<!-- Header
<header >
	<?php the_title(); ?>
</header>

<h2><?php _e('Vos CyberCleanUp', 'cwcud');?></h2>
-->
        
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<section id="raw-content">
	<?php the_content(); ?>
</section>

<main class="wrapper">
	<?php
	$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
	$args = array(
		'paged' => $num_page,
		'post_status'     => 'publish',
		'post_type'			=> 'cleanup'
	);
	
	if(isset($_GET['s_cleanup']) && !empty($_GET['s_cleanup'])){
		$meta_query = array();
		$args['s'] = sanitize_text_field($_GET['s_cleanup']);
		$meta_query['relation'] = "OR";
		$meta_query[] = array(
			'key' => 'structure_name',
			'value' => sanitize_text_field($_GET['s_cleanup']),
			'compare' => 'LIKE'
		);
		$meta_query[] = array(
			'key' => 'cleanup_tag',
			'value' => sanitize_text_field($_GET['s_cleanup']),
			'compare' => 'LIKE'
		);
		$meta_query[] = array(
			'key' => 'cleanup_adresse',
			'value' => sanitize_text_field($_GET['s_cleanup']),
			'compare' => 'LIKE'
		);
		$meta_query[] = array(
			'key' => 'more_information',
			'value' => sanitize_text_field($_GET['s_cleanup']),
			'compare' => 'LIKE'
		);
		$args['meta_query'] = $meta_query;
	}	
	$args2 = $args;
	$args2['paged'] = '';
	$args2['posts_per_page'] = -1;
	query_posts($args2);
	global $wp_query; 
	$nb_cleanup = 0;
	if ( have_posts() ) : 
		while (have_posts()) : the_post();
			if(isset($_GET['s_dep']) && !empty($_GET['s_dep'])){
				if($_GET['s_dep'] == get_post_meta($post->ID, "cleanup_departement", true) ){
					$nb_cleanup++;
				}
			}else{
				$nb_cleanup++;
			}
		endwhile; 
	endif;
	echo '<p><b>Il y a '.$nb_cleanup.' Cleanups</b></p>';
	?>
	<!-- FILTERS -->		
	<form method="GET" class="filters-cleanup">
		<!-- Filters -->
		<div class="filters">
			<!-- par départements -->
			<div class="select">
				<select name="s_dep">
					<option value="">Tous les départements</option>
					<?php
					$tab_departements = arrayDepartements();
					foreach ($tab_departements as $key => $value):?>
						<option value="<?php echo $key;?>" <?php if(isset($_GET['s_dep'])) {selected( $key, $_GET['s_dep'] );} ?>><?php echo $key;?> - <?php echo $value;?> </option>
					<?php endforeach;?>
				</select>
			</div>

			<!-- search -->
			<input type="text" placeholder="Rechercher un CyberCleanUp" name="s_cleanup" value="<?php echo (isset($_GET['s_cleanup']))?$_GET['s_cleanup']:'';?>">
		</div>

		<!-- btn -->
		<input type="submit" class="button" value="Rechercher">

	</form>

	<!-- Listing Archive -->
	
		
		<?php
		

		query_posts($args);
		global $wp_query; 
		if ( have_posts() ) : 
			echo '<section class="listing-cleanup">';
			while (have_posts()) : the_post();
				if(isset($_GET['s_dep']) && !empty($_GET['s_dep'])){
					if($_GET['s_dep'] == get_post_meta($post->ID, "cleanup_departement", true) ){
						part_cleanup($post);
					}
				}else{
					part_cleanup($post);
				}
				
			endwhile; 
			echo '</section>';
			?>
			
			<?php
			ihag_page_navi();
			wp_reset_query();
		else:
			?>
			<p>
				Il n'y a pas de CyberCleanUp correpondant à votre recherche, nous vous invitons à <a href="<?php the_permalink(get_field("page_add_organisateur","option"));?>">créer le premier CyberCleanUp</a> de cette zone.
			</p>
			<?php
		endif; ?>

	<div class="modale" id="modale_inscription_cleanup">
		<p style="text-align:center;cursor:pointer;color:white" class="close-modale">X Fermer</p>
		<div class="cleanup-modale">
			<div id="modale_title_cleanup">
			</div>
			<form class="form-white" action="" method="post" name="registerUserCleanUp" id="registerUserCleanUp">
				<label for="name_registerUserCleanUp"><?php _e('Nom', 'cwcud') ?></label>
				<input type="text" name="name_registerUserCleanUp" id="name_registerUserCleanUp" placeholder="<?php _e('Véronique Dubois', 'cwcud'); ?>">

				<label for="phone_registerUserCleanUp"><?php _e('Téléphone', 'cwcud') ?></label>
				<input type="tel" name="phone_registerUserCleanUp" id="phone_registerUserCleanUp" placeholder="<?php _e('+33 6 01 02 03 04', 'cwcud'); ?>">

				<label for="email_registerUserCleanUp"><?php _e('Adresse e-mail', 'cwcud') ?>*</label>
				<input type="email" name="email_registerUserCleanUp" id="email_registerUserCleanUp" required placeholder="<?php _e('adresse@mail.com', 'cwcud'); ?>">
				<p class="form-info">
					<?php _e('* Votre adresse email sera utilisée pour vous recontacter et vous tenir infomé.', 'cwcud'); ?>
				</p>
				<label for="invite_registerUserCleanUp"><?php _e("Nombre d'invités (en plus de vous)", 'cwcud') ?></label>
				<input type="number" name="invite_registerUserCleanUp" id="invite_registerUserCleanUp" value="" placeholder="Laisser vide si pas d'invité">
				
				<label for="invite_child_registerUserCleanUp" id="invite_child_registerUserCleanUp_label"><?php _e("Nombre d'enfants invités", 'cwcud') ?></label>
				<input type="number" name="invite_child_registerUserCleanUp" id="invite_child_registerUserCleanUp"  value="" placeholder="Laisser vide si pas d'invité">

				<p class="form-item no-margin form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

				<div class="checkbox form-item">
					<input type="checkbox" name="ok_cgu" id="ok_cgu" required>
					<div class="checkbox-label">
						<label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Cyber World CleanUp Day.', 'cwcud');?>*</label>
						<div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
					</div>
				</div>

				<div class="checkbox form-item">
					<input type="checkbox" name="checkbox_newsletter" id="checkbox_newsletter" value="true">
					<div class="checkbox-label">
						<label for="checkbox_newsletter">
							<?php _e("Je désire m'inscrire à la newsletter du CyberWorlCleanUpDay", 'cwcud');?>
						</label>
					</div>
				</div>
				
				<input type="hidden" name="honeypot" value="">
				<input type="hidden" name="id_cleanup" id="id_cleanup" value="">
				<input type="submit" class="button form-item" id="sendRegisterUserCleanUp" value="<?php _e("Valider l'inscription", 'cwcud'); ?>">
				<div id="ResponseMessageRegisterUserCleanUp" class="ResponseMessageRegisterUserCleanUp">
					<p class="ctr"><?php _e('Merci, votre inscription a été enregistrée.', '');?></p>
				</div>
			</form>
		</div>
	</div>

</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); 
?>

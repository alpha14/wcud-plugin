if (document.getElementById("registerUserCleanUp")) {
    //submit form
    document.forms.namedItem("registerUserCleanUp").addEventListener('submit', function(e) {
        document.getElementById('sendRegisterUserCleanUp').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("registerUserCleanUp");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'registerUserCleanUp', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('sendRegisterUserCleanUp').disabled = false;
                document.getElementById('sendRegisterUserCleanUp').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageRegisterUserCleanUp').classList.add("showResponseMessage");
            } else if (xhr.status === 304) {
                console.log("304");
                document.getElementById('sendRegisterUserCleanUp').disabled = false;
                document.getElementById('sendRegisterUserCleanUp').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageRegisterUserCleanUp').innerHTML = "Votre inscription à déjà été prise en compte.";
                document.getElementById('ResponseMessageRegisterUserCleanUp').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}

if (document.getElementById("organisateurForm")) {
    //submit form
    document.forms.namedItem("organisateurForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("organisateurForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'new-orga', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                location.href = page_add_cleanup;
            }
            if (xhr.status === 304) {
                location.href = home_url;
            }
        };
        xhr.send(formData);
    });
}

if (document.getElementById("cleanupForm")) {
    //submit form
    document.forms.namedItem("cleanupForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("cleanupForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'new-cleanup', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                console.log(xhr.response);
                if (xhr.response === 'false') {
                    alert('problème.');
                } else {
                    location.href = page_list_cleanup;
                }
            }
        };
        xhr.send(formData);
    });
}

if (document.getElementById("updateCleanupForm")) {
    //submit form
    document.forms.namedItem("updateCleanupForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("updateCleanupForm");
        var formData2 = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'update-cleanup', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                console.log(xhr.response);
                if (xhr.response === 'false') {
                    alert('problème.');
                } else {
                    if (typeof action_url !== 'undefined') {
                        location.href = action_url;
                    } else {
                        location.href = page_list_cleanup;
                    }

                }
            }
        };
        xhr.send(formData2);
    });
}

function displayRadioValue() {
    var ele = document.getElementsByClassName('structure');
    for (i = 0; i < ele.length; i++) {
        if ((ele[i].id == 'citizen') && (ele[i].checked)) {
            document.getElementById("structure_name").style.display = 'none';
        } else if (ele[i].id == 'citizen') {
            document.getElementById("structure_name").style.display = 'inline-block';
        }
    }
}

function displayDate(e) {
    document.getElementById("date_sec").style.display = 'inline-block';
    if (!e.checked) {
        document.getElementById("date_sec").style.display = 'none';
    }
}

if (document.getElementById("searchAdressMap")) {

    var lat = 48.852969;
    var lon = 2.349903;
    var macarte = null;
    var myMarker = null;
    window.onload = function() {
        AdminInitMap();
    };
    document.getElementById("searchAdressMap").addEventListener("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var formData = new FormData();
        formData.append('address', document.getElementById("cleanup_adresse_id").value);
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'search-address', true);
        console.log(resturl);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                coordinates = JSON.parse(xhr.response);
                if (coordinates[1]) {
                    var newLatLng = new L.LatLng(parseFloat(coordinates[1]), parseFloat(coordinates[0]));
                    myMarker.setLatLng(newLatLng);
                    macarte.setView(myMarker.getLatLng(), macarte.getZoom());
                    document.getElementById("coordonate").value = coordinates[1] + ',' + coordinates[0];
                }
            }
            if (xhr.status === 304) {
                console.log("304");
            }
        };
        xhr.send(formData);
    });
}

// Fonction d'initialisation de la carte
function AdminInitMap() {
    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"

    if (document.getElementById("coordonate").value != "") {
        coordinates = document.getElementById("coordonate").value.split(',')
        lat = parseFloat(coordinates[0]);
        lon = parseFloat(coordinates[1]);
    }

    macarte = L.map('map').setView([lat, lon], 10);

    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);

    myMarker = L.marker([lat, lon], { title: "MyPoint", alt: "The Big I", draggable: true })
        .addTo(macarte)
        .on('dragend', function() {
            var coord = myMarker.getLatLng();
            document.getElementById("coordonate").value = coord['lat'] + "," + coord['lng'];
        });

}

function displayMap() {
    var no = document.getElementById("no_on_map");
    var yes = document.getElementById("yes_on_map");
    if (no.checked) {
        document.getElementById("allMap").style.display = 'none';
    } else if (yes.checked) {
        document.getElementById("allMap").style.display = 'inline-block';
    }
}


function displayInformations() {
    var element = document.getElementsByClassName('location');
    for (var i = 0; i < element.length; i++) {
        if ((element[i].id == 'location_facetoface') && (element[i].checked)) {
            document.getElementById("cleanup_adresse").style.display = 'inline-block';
            document.getElementById("more_information").style.display = 'inline-block';
            document.getElementById("link_connection").style.display = 'none';
        } else if ((element[i].id == 'location_distancing') && (element[i].checked)) {
            //document.getElementById("cleanup_adresse").style.display = 'none';
            document.getElementById("cleanup_adresse").style.display = 'inline-block';
            document.getElementById("more_information").style.display = 'none';
            document.getElementById("link_connection").style.display = 'inline-block';
        } else if ((element[i].id == 'location_both') && (element[i].checked)) {
            document.getElementById("cleanup_adresse").style.display = 'inline-block';
            document.getElementById("more_information").style.display = 'inline-block';
            document.getElementById("link_connection").style.display = 'inline-block';
        }
    }

}

function initMap() {
    // Nous définissons le dossier qui contiendra les marqueurs
    //var iconBase = 'http://localhost/carte/icons/';
    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
    macarte = L.map(elemMap).setView([lat, lon], zoom);
    //markerClusters = L.markerClusterGroup();
    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © OpenStreetMap/ODbL - rendu OSM France',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);
    // Nous parcourons la liste des villes
    var myIconPublic = L.icon({
        iconUrl: iconBase + "picto_public.png",
        iconSize: [34, 42],
        iconAnchor: [17, 42],
        popupAnchor: [-0, -42],
    });
    var myIconPrivate = L.icon({
        iconUrl: iconBase + "picto_private.png",
        iconSize: [34, 42],
        iconAnchor: [17, 42],
        popupAnchor: [-0, -42],
    });
    for (ville in tab_cleanup) {

        coordinates = tab_cleanup[ville].coordonate.split(",");

        lat = parseFloat(coordinates[0]);
        lon = parseFloat(coordinates[1]);

        if (tab_cleanup[ville].etat == "no_private") {
            var marker = L.marker([lat, lon], { icon: myIconPublic }).addTo(macarte);
            //var marker = L.marker([lat, lon], { icon: myIconPublic });
        } else {
            var marker = L.marker([lat, lon], { icon: myIconPrivate }).addTo(macarte);
            //var marker = L.marker([lat, lon], { icon: myIconPrivate });
        }
        marker.bindPopup('<a href="' + tab_cleanup[ville].link + '">' + tab_cleanup[ville].name + '</a><br>' + tab_cleanup[ville].date + '<br>' + tab_cleanup[ville].organisator);
        //markerClusters.addLayer(marker);
    }
    //macarte.addLayer(markerClusters);
}
if (document.getElementById("mapCleanup")) {
    var lat = 46.5;
    var lon = 2.349903;
    var macarte = null;
    var zoom = 6;
    elemMap = "mapCleanup";
    //var markerClusters;
    initMap();
}

if (document.getElementById("single-map-cleanup")) {
    var lat = 46.5;
    var lon = 2.349903;
    for (ville in tab_cleanup) {
        coordinates = tab_cleanup[ville].coordonate.split(",");
        lat = parseFloat(coordinates[0]);
        lon = parseFloat(coordinates[1]);
    }
    var macarte = null;
    var zoom = 12;
    elemMap = "single-map-cleanup";
    document.getElementById("display-map-cleanup").addEventListener("click", function(e) {
        initMap();
    });
}


document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("inscription_cleanup");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();

            document.getElementById("modale_title_cleanup").innerHTML = el.dataset.title;
            document.getElementById("id_cleanup").value = el.dataset.id;

            if (document.getElementById("invite_child_registerUserCleanUp_label")) {
                document.getElementById("invite_child_registerUserCleanUp").type = el.dataset.child;
                document.getElementById("invite_child_registerUserCleanUp_label").style.display = el.dataset.childLabel;
            }
            document.getElementById('modale_inscription_cleanup').classList.add("active");
        });
    });
});


document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("get_list_inscrits");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            var formData = new FormData();
            formData.append('id_cleanup', el.dataset.id);
            xhr = new XMLHttpRequest(); //prepare la requete
            xhr.open('POST', resturl + 'list-inscrits', true);
            xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    document.getElementById("list_inscrits").innerHTML = xhr.response;
                    document.getElementById('modal_list_inscrits').classList.add("active");
                }
            };
            xhr.send(formData);

        });
    });
});

if (document.getElementById("get_modale_orga")) {
    document.getElementById("get_modale_orga").addEventListener("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var formData = new FormData();
        formData.append('id_cleanup', document.getElementById("get_modale_orga").dataset.idCleanup);
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'info-orga', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById("modale_orga_content").innerHTML = xhr.response;
                document.getElementById('modale_orga').classList.add("active");
            }
        };
        xhr.send(formData);
    });
}
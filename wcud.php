<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://ihaveagreen.fr/
 * @since             1.0.0
 * @package           wcud
 *
 * @wordpress-plugin
 * Plugin Name:       World CleanUp day
 * Plugin URI:        https://ihaveagreen.fr/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            I Have a Green
 * Author URI:        https://ihaveagreen.fr/
 * Text Domain:       wcud
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
include_once("include/template.php");
include_once("include/rest.php");
include_once("include/back-office.php");

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WCUD_VERSION', '1.0.0' );


add_action('init', 'ihag_register_roleorganisateur' );
function ihag_register_roleorganisateur() {
	add_role( 'organisateur', __( 'Organisateur	', 'cwcud' ), array( 'read' => true, 'level_0' => true ) );
	//add_role( 'participant', __( 'Participant', 'cwcud' ), array( 'read' => true, 'level_0' => true ) );
}
add_filter( 'user_search_columns', function( $search_columns ) {
	$search_columns[] = 'display_name';
	/*$search_columns[] = 'first_name';
	$search_columns[] = 'last_name';*/
	
    return $search_columns;
} );

function add_table_participants()
{   
	
  	global $wpdb; 
	$db_table_name = $wpdb->prefix . 'participant';  // table name
	$charset_collate = $wpdb->get_charset_collate();
	
	if($wpdb->get_var( "show tables like '$db_table_name'" ) != $db_table_name ) 
	{
		$sql = "CREATE TABLE $db_table_name (
			id int(11) NOT NULL auto_increment,
			email varchar(200) NOT NULL,
			name varchar(200),
			phone varchar(20),
			PRIMARY KEY (id)
		) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
		error_log($sql);
	 }
	 
	$db_table_name = $wpdb->prefix . 'participant_cleanup';  // table name
  	$charset_collate = $wpdb->get_charset_collate();
	if($wpdb->get_var( "show tables like '$db_table_name'" ) != $db_table_name ) 
	{
		$sql = "CREATE TABLE $db_table_name (
			id_participant int(11) NOT NULL,
			id_cleanup int(11) NOT NULL,
			guest int(11) NOT NULL,
			child int(11) NOT NULL
		) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
 	}
} 
register_activation_hook( __FILE__, 'add_table_participants' );

add_action('wp_enqueue_scripts', 'ihag_add_asset', 999);
function ihag_add_asset()
{
	$plugin_data = get_plugin_data( __FILE__ );
	$plugin_name = $plugin_data['Name'];
    global $wp_styles;
    wp_enqueue_style('wcud', plugin_dir_url( __FILE__ ) . 'assets/style.css', array('styles'));

    wp_register_script('wcud', plugin_dir_url( __FILE__ ) . 'assets/script.js', array('script'), false, 'all');
    wp_enqueue_script('wcud');
	wp_localize_script('wcud', 'page_add_cleanup', get_permalink( get_field("page_add_cleanup", "option") ));
	wp_localize_script('wcud', 'page_update_cleanup', get_permalink( get_field("page_update_cleanup", "option") ));
	wp_localize_script('wcud', 'page_list_cleanup', get_permalink( get_field("page_list_cleanup", "option") ));
	wp_localize_script('wcud', 'home_url', home_url());
	wp_localize_script('wcud', 'iconBase', plugin_dir_url( __FILE__ ) . 'assets/', array('script'));
	
}

add_action( 'init', 'ihag_custom_post_type');
function ihag_custom_post_type() {
	register_post_type( 'cleanup', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array('labels' 			=> array(
				'name' 				=> __('CleanUp', 'ihag'), /* This is the Title of the Group */
				'singular_name' 	=> __('CleanUp', 'ihag'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-flag', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'show_ui'            => true,
			'show_in_menu'       => true,
			'supports' 			=> array( 'title', 'editor',  'custom-fields'),
			'public' => true,
			'show_in_rest' => true,
			'has_archive' => true,
			'publicly_queryable' => true,
	 	) /* end of options */
	);
	//flush_rewrite_rules();
}


add_filter('single_template', 'ihag_use_single_cleanup');
function ihag_use_single_cleanup($single) {

    global $post;

    /* Checks for single template by post type */
    if ( $post->post_type == 'cleanup' ) {
        if ( file_exists( plugin_dir_path( __FILE__ ) . '/templates/single-cleanup.php' ) ) {
            return plugin_dir_path( __FILE__ ) . '/templates/single-cleanup.php' ;
        }
    }
    return $single;
}

function ihag_update_cleanup( $post_id, $post, $update )  {

	if( ! $update ) { return; }
	if( wp_is_post_revision( $post_id ) ) { return; }
	if( defined( 'DOING_AUTOSAVE' ) and DOING_AUTOSAVE ) { return; }
	if( $post->post_type != 'cleanup' ) { return; }
	if (wp_is_post_autosave($post_id)) {return; }
	$tab_participant = get_post_meta( $post_id, 'participants', true );
	if ( empty( $tab_participant ) ) {return;}

	foreach ($tab_participant as $participant) {
		$user = get_userdata($participant);
		//error_log($user->user_email);
		$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
		$to = $user->user_email;
		$subject = __("Le CyberCleanUp auquel vous êtes inscrit a été mis à jour",'cwcud');
		$body = "Cyber World CleanUp Day<br><br>
Bonjour ".sanitize_text_field( $user->display_name ) ."<br>
Le CyberCleanUp ".get_the_title($post_id)." auquel vous êtes inscrit a été mis à jour.<br><br>
Rappel du CyberCleanUp :<br>
● ".get_the_title($post_id)."<br>
● Le ".date_i18n('j F Y', strtotime(get_post_meta( $post_id, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post_id, "time_start", true )))." (".get_post_meta($post_id, "time_end", true ).")<br>
● ".get_post_meta($post_id, "cleanup_adresse", true)."<br>";
		$whichLocation = get_post_meta( $post_id, "location", true );
		if ($whichLocation === "location_facetoface"):
			$body .= '● Évènement présentiel : '.get_post_meta( $post_id, "cleanup_adresse", true )."<br>";
		elseif($whichLocation === "location_distancing"):
			$body .= '● Évènement distanciel : '.get_post_meta( $post_id, "link_connection", true )."<br>";
		elseif($whichLocation === "location_both"):
			$body .= '● Évènement présentiel : '.get_post_meta( $post_id, "cleanup_adresse", true )."<br>";
			$link = (!empty(get_post_meta($post_id, "link_connection", true))) ? get_post_meta($post_id, "link_connection", true) : 'Le lien vous sera envoyé ultérieurement par email.';
			$body .= '● Évènement distanciel : '.$link."<br>";
		endif;

		$more_information = get_post_meta( $post_id, "more_information", true ) ;
		if( isset($more_information) && !empty($more_information)){
			$body .= get_post_meta($post_id, "more_information", true )."<br><br>";
		}
		$body .= "Vous êtes susceptible de recevoir des informations complémentaires de la part le
l’organisateur. Sachez que vous pouvez à tout moment modifier et/ou supprimer vos
données participant.
Numériquement Vôtre,<br>
l'équipe du Cyber World CleanUp Day<br><br>
Vous recevez ce mail car vous prenez part au Cyber World CleanUp Day 2021 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@cyberworldcleanupday.fr";
	
		wp_mail( $to, $subject, $body, $headers);
	}
}
add_action( 'save_post_cleanup', 'ihag_update_cleanup', 10, 3 );

function part_cleanup($post){
	?>
	<div class="cleanup-card no-useless-margin">
		<?php
		$arrayDepartements = arrayDepartements();

		echo '<a class="h3-like" href="'.get_the_permalink().'">';
		$is_private = get_post_meta( $post->ID, "private", true );
		if ( $is_private == "no_private" ) {
			echo '<img src="'.plugin_dir_url( __FILE__ ) . 'assets/picto_public.png" width="20"> '; 
			the_title();			
		}
		else{
			echo '<img src="'.plugin_dir_url( __FILE__ ) . 'assets/picto_private.png"  width="20"> '; 
			the_title();
		}
		echo '</a>';
		$author_id = $post->post_author;
		$user = get_userdata( $author_id );
		echo '<p>'.$user->first_name." ".$user->last_name;
		echo (!empty(get_post_meta($post->ID,'structure_name', true))) ? ' - '.get_post_meta($post->ID,'structure_name', true) : '';
		echo '<p>';

		echo '<p class="cleanup-date">';
		echo "Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")<br>";
		echo '<br>'.$arrayDepartements[get_post_meta( $post->ID, "cleanup_departement", true )] . " - ".get_post_meta( $post->ID, "cleanup_departement", true );
		echo '</p>';
		?>
		<p>
		<?php echo wp_trim_words(get_the_content(), 25);?>
		</p>
		<?php 
		if(get_post_meta( $post->ID, "private", true ) == "no_private"):
			$whichLocation = get_post_meta( $post->ID, "location", true ) ;
			$adresse = get_post_meta($post->ID, "cleanup_adresse", true);
			$link = (!empty(get_post_meta($post->ID, "link_connection", true))) ? get_post_meta($post->ID, "link_connection", true) : 'Le lien vous sera visible ultérieurement.';
			$otherInformation = get_post_meta( $post->ID, "more_information", true );

			if ($whichLocation === "location_facetoface"):
			echo '<p>Évènement présentiel : '.$adresse.'<br>'.$otherInformation.'</p>';
			elseif($whichLocation === "location_distancing"):
			echo '<p>Évènement distanciel : '.$link.'</p>';
			elseif($whichLocation === "location_both"):
			echo '<p>Évènement présentiel : '.$adresse.' - '.$otherInformation;
			echo '<br>Évènement distanciel : '.$link.'<p>';
			endif;
		endif;
		?>

		<?php
		if(get_post_meta( $post->ID, "private", true ) == "yes_private"){
			echo '<p><strong>Le CyberCleanUp est privé</strong></p>';
		}
		else{
			$participants = (int)get_post_meta( $post->ID, 'participants', true );
			$nb_participant_max = (int)get_post_meta( $post->ID, "nb_participant_max", true );
			if(empty($nb_participant_max) || $participants < $nb_participant_max):?>
				<button class="button-yellow inscription_cleanup" data-id="<?php echo get_the_ID();?>" data-child="<?php echo (get_post_meta($post->ID, "child", true) == "yes_child") ? "number":"hidden";?>" data-child-label="<?php echo (get_post_meta($post->ID, "child", true) == "yes_child") ? "inline-block":"none";?>" 
					data-title="<h2>Inscription : <?php echo str_replace('"', '', get_the_title());?></h2><p>Organisé par <?php echo get_post_meta($post->ID, "structure_name", true);?>, le <?php echo date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")";?></p>">
					<?php _e('Participer', 'cwcud'); ?>
				</button>
			<?php 
			else:
				echo '<p><strong>Le CyberCleanUp est complet</strong></p>';
			endif;
		}
		
	echo '</div>';
}

add_filter('private_title_format', 'removePrivatePrefix'); 
add_filter('protected_title_format', 'removePrivatePrefix');
function removePrivatePrefix($format) {
return '%s';
}

function get_during(){
	return array(
		"",
		"0h30",
		"1h00",
		"1h30",
		"2h00",
		"2h30",
		"3h00",
		"3h30",
		"4h00",
		"4h30",
		"5h00",
		"5h30",
		"6h00",
		"6h30",
		"7h00",
		"7h30",
		"8h00",
		"8h30",
		"9h00",
		"9h30",
		"10h00",
		"10h30",
		"11h00",
		"11h30",
		"12h00",
	);
}


function ihag_no_admin_access()
{
    if ( !is_admin() || (is_user_logged_in() && isset( $GLOBALS['pagenow'] ) AND 'wp-login.php' === $GLOBALS['pagenow'] ) ) {
        return;
    }

	$redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url( '/' );
	if(!current_user_can('administrator')){
		exit( wp_redirect( $redirect ) );
	}
    
}
add_action( 'admin_init', 'ihag_no_admin_access', 100 );
<?php
use \Mailjet\Resources;
/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'new-orga',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wcud_new_orga',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ihag', 'new-cleanup',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wcud_new_cleanup',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ihag', 'search-address',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wcud_search_address',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ihag', 'update-cleanup',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_update_cleanup',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	register_rest_route( 'ihag', 'registerUserCleanUp',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_registerUserCleanUp',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);

	register_rest_route( 'ihag', 'nbRegisterUserCleanUp',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_nbRegisterUserCleanUp',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	register_rest_route( 'ihag', 'list-inscrits',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_list_inscrits',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);

	register_rest_route( 'ihag', 'info-orga',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_info_orga',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);

	
	
});

function wcud_new_orga(WP_REST_Request $request){
    if (empty($_POST['honeyPot'])) {
        //Génerer un PWD
		$pwd = wp_generate_password();

		// Création de l’utilisateur
		$infos = array(
			'first_name'   => sanitize_text_field( $_POST['user_firstname'] ),
			'last_name'    => sanitize_text_field( $_POST['user_lastname'] ),
			'user_login'   => sanitize_email($_POST['user_email']),
			'user_email'   => sanitize_email($_POST['user_email']),
			'user_pass'    => $pwd,
			'role'         => 'organisateur',
			'display_name' => sanitize_text_field( $_POST['user_email'] ),
		);
		$user_id = wp_insert_user( $infos );
		
		if ( is_wp_error( $user_id ) ) { 
			
			$user = get_user_by( 'email', sanitize_email($_POST['user_email']) );
			if ( $user ) {//user existe déjà, on lui affecte le role organisateur
				$user_id = $user->ID;
				$user_roles = $user->roles;
				// Check if the role you're interested not in
				if ( !in_array( 'organisateur', $user_roles, true ) ) {
					if(sizeof($user_roles) == 1 && ($user_roles[0] == "ambassadeur" || $user_roles[0] == "ambassadeur_to_validate")){
						wp_set_password($pwd, $user_id);

						$creds = array();
						$creds['user_login']    = sanitize_email($_POST['user_email']);
						$creds['user_password'] = $pwd;
						$creds['remember']      = true;
						$user = wp_signon( $creds, false );
					}
					$user->add_role( 'organisateur' );
				}
				else{//user déjà existant avec le role organisateur
					return new WP_REST_Response( '', 304 );
				}
				
			} else {//error 
				return new WP_REST_Response( '', 304 );
			}
		}

		// add user meta
		add_user_meta($user_id, 'user_gender', sanitize_text_field( $_POST['user_gender'] ));
		add_user_meta($user_id, 'user_phone', sanitize_text_field( $_POST['user_phone'] ));
		add_user_meta($user_id, 'adherent_cwcud', sanitize_text_field( $_POST['adherent_cwcud'] ));
		add_user_meta($user_id, 'adherent_inr', sanitize_text_field( $_POST['adherent_inr'] ));


		//Je log automatiquement l’utilisateur s'il n'est pas connecté
		$current_user = wp_get_current_user();
    	if ( 0 == $current_user->ID ) {
			$creds = array();
			$creds['user_login']    = sanitize_email($_POST['user_email']);
			$creds['user_password'] = $pwd;
			$creds['remember']      = true;
			$user = wp_signon( $creds, false );
		}

		$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
		$to = sanitize_email($_POST['user_email']);
		$subject = __("Votre inscription en tant qu'organisateur pour le Cyber World CleanUp Day a été enregistrée",'cwcud');
		$body = "Cyber World CleanUp Day<br><br>
Bonjour ".$_POST['user_firstname']." ".$_POST['user_lastname']."<br><br>
Vous venez de vous inscrire en tant qu'organisateur pour le Cyber World CleanUp Day.<br><br>
Vous pouvez désormais <a href=".get_the_permalink(get_field('page_add_cleanup', 'options')).">organiser un CyberCleanUp</a><br>
Les coordonnées communiquées sont :<br>
● ".sanitize_email($_POST['user_email'])."<br>
● ".sanitize_text_field( $_POST['user_phone'] )."<br><br>
Les identifiants de connexion sont :<br>
● ".sanitize_email($_POST['user_email'])."<br>
● ".$pwd."<br><br>
Sachez que vous pouvez à tout moment <a href=".get_the_permalink(get_field('page_my_account','option')).">modifier votre mot de passe</a> . <br><br>
Nous organisons un webinaire hebdomadaire tous les vendredi de 12h00 à 13h00 pour répondre à vos questions et partager les bonnes pratiques entre organisateurs. Retrouvez-vous sur le lien ci-dessous :<br> 
● https://us02web.zoom.us/j/84318996776?pwd=SUhlNWtvVEZmWXVKdW5ZSWxveTcwdz09<br>
● ID de réunion : 843 1899 6776<br>
● Code secret : 838762<br><br>
Nous ne manquerons pas de vous contacter dans les plus brefs délais afin de valider avec vous ce rôle si gratifiant et porteur d'énergie positive.<br><br>
Numériquement Vôtre,<br>
l'équipe du Cyber World CleanUp Day<br><br>
Vous recevez ce mail car vous prenez part au Cyber World CleanUp Day 2021 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@cyberworldcleanupday.fr";
	
		wp_mail( $to, $subject, $body, $headers);

		require dirname(__DIR__, 1).'/vendor/autoload.php';

		$mj = new \Mailjet\Client(get_field('id_mailjet', 'option'), get_field('mdp_mailjet', 'option'),true,['version' => 'v3']);
		$body = [
		'Action' => "addnoforce",
		'Contacts' => [
				[
				'Email' => sanitize_email($_POST['user_email']),
				'IsExcludedFromCampaigns' => "false",
				'Name' => sanitize_email($_POST['user_email']),
				'Properties' => "object"
				]
			]
		];
		$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => get_field('id_liste_organisateur', 'option'), 'body' => $body]);
		$response->success() && var_dump($response->getData());



    }    
    return new WP_REST_Response( '', 200 );
}

function wcud_new_cleanup(WP_REST_Request $request){
	if ( check_nonce() ) {
		$current_user = wp_get_current_user();
		if (empty($_POST['honeyPot'])) {
			$post['post_type']   = 'cleanup';
			if(isset($_POST['onMap']) && $_POST['onMap'] == "yes_on_map"){
				$post['post_status'] = 'publish';
			}
			else{
				$post['post_status'] = 'private';
			}
			$post['post_title'] = sanitize_text_field($_POST['cleanup_name']);
			$post['post_author'] = $current_user->ID;
			$post['post_content'] = sanitize_textarea_field( $_POST['description']);
			$post_id = wp_insert_post( $post, true );
			ihag_update_post_meta($post_id);

			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			$to = $current_user->user_email;
			$subject = __("Votre CyberCleanUp a été créé - Cyber World CleanUp Day",'cwcud');
			
			$body = "Cyber World CleanUp Day<br><br>

Bonjour ".$current_user->first_name." ".$current_user->last_name."<br><br>
Vous venez d'inscrire votre CyberCleanUp dont voici les détails :<br>
● Nom du CyberCleanUp : ".sanitize_text_field($_POST['cleanup_name'])."<br>
● ID du CyberCleanUp : ".$post_id."<br>
● Date du CyberCleanUp : Le ".date_i18n('j F Y', strtotime($_POST['date_start'] ))." à ".date_i18n('H:i', strtotime($_POST['time_start'] ))." (".$_POST['time_end'].")<br>";
			$whichLocation = $_POST['location'];
			if ($whichLocation === "location_facetoface"): 
				$body .= '● Évènement présentiel : '.$_POST['cleanup_adresse']."<br>";
			elseif($whichLocation === "location_distancing"):
				if(empty($_POST['link_connection'])):
					$body .= '● Évènement distanciel : le lien vous sera envoyé ultérieurement par email.'."<br>";
				else:
					$body .= '● Évènement distanciel : '.$_POST['link_connection']."<br>";
				endif;
			elseif($whichLocation === "location_both"):
				$body .= '● Évènement présentiel : '.$_POST['cleanup_adresse']."<br>";
				if(empty($_POST['link_connection'])):
					$body .= '● Évènement distanciel : le lien vous sera envoyé ultérieurement par email.'."<br>";
				else:
					$body .= '● Évènement distanciel : '.$_POST['link_connection']."<br>";
				endif;
			endif;

			if(isset($_POST['more_information']) && !empty($_POST['more_information'])){
				$body .= $_POST['more_information']."<br>";
			}

			$body .= '● '.nl2br(sanitize_textarea_field($_POST['description']))."<br><br>";
			$body .= "Vous pouvez désormais inviter vos amis et collègues à s'inscrire à votre CyberCleanUp en
communiquant le lien ci-dessous :<br>
● ".get_permalink($post_id)."<br><br>
Sachez que vous pouvez à tout moment modifier et/ou supprimer votre CyberCleanUp en cliquant sur ce lien : ".get_permalink(get_field('page_list_cleanup','option'))."<br><br>
Ce lien vous permet également de savoir qui s'inscrit. Ne manquez pas de leur communiquer des informations qui vous semblent importantes ( modification de CyberCleanUp, auberge espagnole, matériel à emporter...)<br>
Nous vous invitons à conserver ce mail pour toute modification ultérieure<br><br>
Numériquement Vôtre,<br>
l'équipe du Cyber World CleanUp Day<br><br>
Vous recevez ce mail car vous prenez part au Cyber World CleanUp Day 2021 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@cyberworldcleanupday.fr";
		
			wp_mail( $to, $subject, $body, $headers);

			if ( !is_wp_error( $post_id ) ) { 
				return new WP_REST_Response( '', 200 );
			}
		} 
		
	}
    return new WP_REST_Response( '', 304 );
}

function wcud_update_cleanup(WP_REST_Request $request){	
	if (empty($_POST['honeyPot'])) {
		$post_id = $_POST['id'];
		$post = array (
			'ID' 		 => $post_id,
			'post_title' => $_POST['cleanup_name'],
		);
		if(isset($_POST['onMap']) && $_POST['onMap'] == "yes_on_map"){
			$post['post_status'] = 'publish';
		}
		else{
			$post['post_status'] = 'private';
		}
		$post['post_content'] = sanitize_textarea_field( $_POST['description']);
		wp_update_post($post);
		ihag_update_post_meta($post_id);	
		if ( !is_wp_error( $post_id ) ) { 
			return new WP_REST_Response( '', 200 );
		}
	}
}

function ihag_update_post_meta($post_id){
	update_post_meta($post_id, 'type_structure', sanitize_text_field( $_POST['type_structure'] ));
	if (isset($_POST['structure_name'])){
		update_post_meta($post_id, 'structure_name', sanitize_text_field( $_POST['structure_name'] ));
	}
	//update_post_meta($post_id, 'cleanup_name', sanitize_text_field( $_POST['cleanup_name'] ));
	//update_post_meta($post_id, 'description', sanitize_textarea_field( $_POST['description']));
	update_post_meta($post_id, 'date_start', sanitize_text_field( $_POST['date_start'] ));
	update_post_meta($post_id, 'time_start', sanitize_text_field( $_POST['time_start'] ));
	//update_post_meta($post_id, 'need_second_date', sanitize_text_field( $_POST['need_second_date'] ));
	//if (isset($_POST['date_end'])){
		update_post_meta($post_id, 'time_end', sanitize_text_field( $_POST['time_end'] ));
	//}
	update_post_meta($post_id, 'cleanup_tag', sanitize_text_field( $_POST['cleanup_tag'] ));
	update_post_meta($post_id, 'onMap', sanitize_text_field( $_POST['onMap'] ));
	update_post_meta($post_id, 'private', sanitize_text_field( $_POST['private'] ));
	update_post_meta($post_id, 'child', sanitize_text_field( $_POST['child'] ));
	update_post_meta($post_id, 'nb_participant_max', sanitize_text_field( $_POST['nb_participant_max'] ));
	update_post_meta($post_id, 'user_phone', sanitize_text_field( $_POST['user_phone'] ));
	update_post_meta($post_id, 'user_email', sanitize_text_field( $_POST['user_email'] ));
	update_post_meta($post_id, 'location', sanitize_text_field( $_POST['location'] ));
	if (isset($_POST['coordonate']) && !empty($_POST['coordonate'])){
		update_post_meta($post_id, 'coordonate', sanitize_text_field( $_POST['coordonate'] ));
	}else{//empty
		update_post_meta($post_id, 'coordonate', "48.85302176261338,2.349699251353741" );
	}
	
	if (isset($_POST['cleanup_adresse'])){
		update_post_meta($post_id, 'cleanup_adresse', sanitize_text_field( $_POST['cleanup_adresse'] ));
	}
	update_post_meta($post_id, 'cleanup_departement', sanitize_text_field( $_POST['cleanup_departement'] ));
	if (isset($_POST['more_information'])){
		update_post_meta($post_id, 'more_information', sanitize_text_field( $_POST['more_information'] ));
	}
	if (isset($_POST['link_connection'])){
		update_post_meta($post_id, 'link_connection', sanitize_text_field( $_POST['link_connection'] ));
	}

}

function wcud_search_address(WP_REST_Request $request){
	if ( check_nonce() ) {

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api-adresse.data.gouv.fr/search/?q=".str_replace(" ", "+", $_POST["address"]));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);	

		$r = json_decode( curl_exec($ch) );
		$r = (empty($r->features[0]->geometry->coordinates)) ? '' : $r->features[0]->geometry->coordinates ;
		curl_close($ch);
		return new WP_REST_Response( $r, 200 );
	}
	return new WP_REST_Response( '', 304 );
}


function wcud_registerUserCleanUp(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {
			global $wpdb;

			$db_item =  $wpdb->get_results($wpdb->prepare(
				"SELECT id, name, phone FROM {$wpdb->prefix}participant WHERE email LIKE %s", sanitize_email($_POST['email_registerUserCleanUp'])
			));
			if($wpdb->num_rows == 0){
				$wpdb->insert(
					$wpdb->prefix.'participant',
					array(
						'email' => sanitize_email($_POST['email_registerUserCleanUp']),
						'name'	=> sanitize_text_field( $_POST['name_registerUserCleanUp'] ),
						'phone'	=> sanitize_text_field( $_POST['phone_registerUserCleanUp'] ),
					),
					array(
						'%s',
						'%s',
						'%s'
					)
				);
				$id_participant = $wpdb->insert_id;
			}elseif($wpdb->num_rows == 1){
				$wpdb->update(
					$wpdb->prefix.'participant',
					array(
						'name'	=> ( (!empty(sanitize_text_field( $_POST['name_registerUserCleanUp']) ) ) ? sanitize_text_field( $_POST['name_registerUserCleanUp']) : $db_item[0]->name),
						'phone'	=> ( (!empty(sanitize_text_field( $_POST['phone_registerUserCleanUp'])) ) ? sanitize_text_field( $_POST['phone_registerUserCleanUp']) : $db_item[0]->phone),
					),
					array('id' => $db_item[0]->id)
				);
				$id_participant = $db_item[0]->id;
			}

			$db_item =  $wpdb->get_results($wpdb->prepare(
				"SELECT * FROM {$wpdb->prefix}participant_cleanup WHERE id_participant = %d AND id_cleanup = %d" , array( $id_participant, sanitize_text_field($_POST['id_cleanup']))
			));
			if($wpdb->num_rows == 0){
				$wpdb->insert(
					$wpdb->prefix.'participant_cleanup',
					array(
						'id_participant' => $id_participant,
						'id_cleanup'	=> $_POST['id_cleanup'],
						'guest'	=> ( (int)sanitize_text_field ( $_POST['invite_registerUserCleanUp'] ) + 1),
						'child'	=> (int)sanitize_text_field( $_POST['invite_child_registerUserCleanUp'] ),
					),
					array(
						'%d',
						'%d',
						'%d',
						'%d'
					)
				);
			}
			elseif($wpdb->num_rows == 1){
				$wpdb->update(
					$wpdb->prefix.'participant_cleanup',
					array(
						'guest'	=> ( (int)sanitize_text_field ( $_POST['invite_registerUserCleanUp'] ) + 1),
						'child'	=> (int)sanitize_text_field( $_POST['invite_child_registerUserCleanUp'] ),
					),
					array(
						'id_participant' => $id_participant,
						'id_cleanup' => sanitize_text_field($_POST['id_cleanup']),
					)
				);
			}

			$db_item =  $wpdb->get_results($wpdb->prepare(
				"SELECT sum(guest) as guest, sum(child) as child FROM {$wpdb->prefix}participant_cleanup WHERE id_cleanup = %d" , array( sanitize_text_field($_POST['id_cleanup']))
			));
			update_post_meta($_POST['id_cleanup'], 'participants', ($db_item[0]->guest + $db_item[0]->child));
			
			

			if(isset($_POST['checkbox_newsletter']) && $_POST['checkbox_newsletter'] == "true"){
				require dirname(__DIR__, 1).'/vendor/autoload.php';

				$mj = new \Mailjet\Client(get_field('id_mailjet', 'option'), get_field('mdp_mailjet', 'option'),true,['version' => 'v3']);
				$body = [
				'Action' => "addnoforce",
				'Contacts' => [
						[
						'Email' => sanitize_email($_POST['email_registerUserCleanUp']),
						'IsExcludedFromCampaigns' => "false",
						'Name' => sanitize_email($_POST['email_registerUserCleanUp']),
						'Properties' => "object"
						]
					]
				];
				$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => get_field('id_liste', 'option'), 'body' => $body]);
				$response->success() && var_dump($response->getData());
			}

			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			$to = sanitize_email($_POST['email_registerUserCleanUp']);
			$subject = __("Votre participation au CyberWCUD a été enregistrée",'cwcud');
			$body = "
Bonjour ".sanitize_text_field( $_POST['name_registerUserCleanUp'] ) ."<br><br>
Votre participation au CyberCleanUp ".get_the_title($_POST['id_cleanup'])." a été enregistrée.<br><br>
Rappel du CyberCleanUp :<br>
● ".get_the_title($_POST['id_cleanup'])."<br>
● Le ".date_i18n('j F Y', strtotime(get_post_meta( $_POST['id_cleanup'], "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($_POST['id_cleanup'], "time_start", true )))." (".get_post_meta($_POST['id_cleanup'], "time_end", true ).")<br>
● ".get_post_meta($_POST['id_cleanup'], "cleanup_adresse", true)."<br>";
			$whichLocation = get_post_meta( $_POST['id_cleanup'], "location", true );
			if ($whichLocation === "location_facetoface"):
				$body .= '● Évènement présentiel : '.get_post_meta( $_POST['id_cleanup'], "cleanup_adresse", true )."<br>";
			elseif($whichLocation === "location_distancing"):
				if(empty(get_post_meta( $_POST['id_cleanup'], "link_connection", true ))):
					$body .= '● Évènement distanciel : le lien vous sera envoyé ultérieurement par email.'."<br>";
				else:
					$body .= '● Évènement distanciel : '.get_post_meta( $_POST['id_cleanup'], "link_connection", true )."<br>";
				endif;

			elseif($whichLocation === "location_both"):
				$body .= '● Évènement présentiel : '.get_post_meta( $_POST['id_cleanup'], "cleanup_adresse", true )."<br>";
				$link = (!empty(get_post_meta($_POST['id_cleanup'], "link_connection", true))) ? get_post_meta($_POST['id_cleanup'], "link_connection", true) : 'Le lien vous sera envoyé ultérieurement par email.';
				$body .= '● Évènement distanciel : '.$link."<br>";
			endif;

			$more_information = get_post_meta( $_POST['id_cleanup'], "more_information", true ) ;
			if( isset($more_information) && !empty($more_information)){
				$body .= get_post_meta( $_POST['id_cleanup'], "more_information", true )."<br><br>";
			}
			$body .= "<br>Vous êtes susceptible de recevoir des informations complémentaires de la part le l’organisateur.<br>Sachez que vous pouvez à tout moment supprimer vos données participant.<br> Lien de désinscription : "; 
			$tab_arg = array(
				'remove_email' => sanitize_email($_POST['email_registerUserCleanUp']),
				'key'	=> crypt(sanitize_email($_POST['email_registerUserCleanUp']), 'windows95'),
			);
			$body .= esc_url( add_query_arg( $tab_arg, get_the_permalink($_POST['id_cleanup'])));

$body .= "<br><br>Numériquement Vôtre,<br>
l'équipe du Cyber World CleanUp Day<br><br>
Vous recevez ce mail car vous prenez part au Cyber World CleanUp Day 2021 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@cyberworldcleanupday.fr";
		
			wp_mail( $to, $subject, $body, $headers);
			
		}
    }    
    return new WP_REST_Response( '', 200 );
}

function wcud_nbRegisterUserCleanUp(){
	$tab_participant = get_post_meta( $_POST['id_cleanup'], 'participants', true );
	if ( empty( $tab_participant ) ) {
		return new WP_REST_Response( '0', 200 );
	}
	return new WP_REST_Response( sizeof($tab_participant), 200 );
}

function wcud_list_inscrits(){
	/*$tab_participant = get_post_meta( $_POST['id_cleanup'], 'participants', true );
	if ( empty( $tab_participant ) ) {
		return new WP_REST_Response( '', 200 );
	}*/
	global $wpdb;
	$tab_participant =  $wpdb->get_results($wpdb->prepare(
		"SELECT name, phone, email, guest, child FROM {$wpdb->prefix}participant INNER JOIN {$wpdb->prefix}participant_cleanup ON {$wpdb->prefix}participant.id = {$wpdb->prefix}participant_cleanup.id_participant WHERE id_cleanup = %d", sanitize_text_field($_POST['id_cleanup'])
	));
	echo '<h2>Liste des participants à '.get_the_title($_POST['id_cleanup']).'</h2>';
	foreach($tab_participant as $participant){
		$u = get_userdata($user_id);
		echo $participant->name.' - ';
		echo $participant->email;
		if(!empty($participant->phone)){
			echo ' - tel: '.$participant->phone;
		}
		if($participant->guest > 0){
			echo ' - participants : '.$participant->guest;
		}
		if($participant->child > 0){
			echo ' - participants enfants: '.$participant->child;
		}
		echo '<br>';

	}
	return new WP_REST_Response( NULL, 200 );
}

function wcud_info_orga(){
	$post =  get_post($_POST["id_cleanup"]);
	$author_id = $post->post_author;
	$user = get_userdata( $author_id );
	echo '<h2>'.get_the_title($post->ID).'</h2>';

	echo '<p><b>Organisé par</b> : ';

	echo $user->first_name.' '.$user->last_name;

	$structure_name = get_post_meta($post->ID,'structure_name', true);
	if(!empty($structure_name)){
		echo ' - '.$structure_name;
	}


	echo "<br>Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")</p>";

	echo '<p><b>Adresse email</b> : '.get_post_meta($post->ID,'user_email', true).'</p>';

	echo '<p><b>Téléphone</b> : '.get_post_meta($post->ID,'user_phone', true).'</p>';

	//get_the_author_meta( string $field = '', int|false $user_id = false )
	return new WP_REST_Response( NULL, 200 );
}
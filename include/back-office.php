<?php

/*
function new_contact_methods( $contactmethods ) {
    $contactmethods['phone'] = 'Téléphone';
    $contactmethods['user_email'] = 'test';
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'new_contact_methods', 10, 1 );
*/

function new_modify_user_table( $column ) {
    $column['phone'] = 'Téléphone';
    $column['nb_cleanup'] = 'Nbr cleanUp';
    unset($column['email']);
    unset($column['posts']);
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'phone' :
            return get_the_author_meta( 'user_phone', $user_id );
        case 'nb_cleanup' :
            return '<a href="'.admin_url().'edit.php?post_type=cleanup&author='.$user_id.'">'.count_user_posts( $user_id, 'cleanup' ).'</a>';
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );




add_action('admin_menu', 'ihag_user_sub_menu_role');
function ihag_user_sub_menu_role() {
    add_submenu_page(
        'users.php',
        'ambassadeur_to_validate',
        'Ambassadeurs à valider',
        'manage_options',
        'users.php?role=ambassadeur_to_validate',
    );

    add_submenu_page(
        'users.php',
        'ambassadeur',
        'Ambassadeurs',
        'manage_options',
        'users.php?role=ambassadeur',
    );

    add_submenu_page(
        'users.php',
        'organisateur',
        'Organisateurs',
        'manage_options',
        'users.php?role=organisateur',
    );
    add_submenu_page(
        'users.php',
        'map_ambassadeur',
        'Carte des ambassadeurs',
        'manage_options',
        'map_ambassadeur',
        'map_ambassadeur',
    );
    global $submenu;
    unset($submenu[ 'users.php' ][16]);
}



add_action('show_user_profile', 'ihag_custom_user_profile_fields_organisateur');
add_action('edit_user_profile', 'ihag_custom_user_profile_fields_organisateur');
function ihag_custom_user_profile_fields_organisateur( $user ) {
    if ( in_array( 'organisateur', $user->roles, true )) {
?>
    <h2>Organisateur</h2>
    <table class="form-table">
        
        <tr>
            <th><label for="user_phone">Téléphone</label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr(get_user_meta($user->ID, 'user_phone', true)); ?>"
                    name="user_phone"
                    id="user_phone"
                    class="regular-text"
                >
            </td>
        </tr>
        <tr>
            <th>
                <label for="user_gender">Genre</label>
            </th>
            <td>
                <div class="form-row">
                    <div class="checkbox">
                        <input type="radio" name="user_gender" class="gender" id="user_female" value="user_female" <?php checked( get_user_meta($user->ID , 'user_gender', true ), "user_female");?> >
                        <label class="checkbox-label"  for="user_female"><?php _e('Madame', 'cwcud');?></label>
                    </div>
                    <div class="checkbox">
                        <input type="radio" name="user_gender" class="gender" id="user_male" value="user_male"  <?php checked( get_user_meta($user->ID , 'user_gender', true ), "user_male");?>>
                        <label class="checkbox-label" for="user_male"><?php _e('Monsieur', 'cwcud');?></label>
                    </div>
                </div>
            </td>
        </tr>
        
        

    </table>
<?php
    } 
}

add_action( 'personal_options_update', 'update_extra_profile_fields_organisateur' );
add_action( 'edit_user_profile_update', 'update_extra_profile_fields_organisateur' );
function update_extra_profile_fields_organisateur( $user_id ) {
    if ( current_user_can( 'edit_user', $user_id ) ){
        update_user_meta( $user_id, 'user_phone', $_POST['user_phone'] );        
        update_user_meta( $user_id, 'user_gender', $_POST['user_gender'] );        
    }
}



add_filter( 'manage_cleanup_posts_columns', 'ihag_manage_column_cleanup',998 );
function ihag_manage_column_cleanup($columns) {
   /* unset( $columns['date'] );
    unset( $columns['wpseo-score'] );
    unset( $columns['wpseo-score-readability'] );
    unset( $columns['wpseo-links'] );
    unset( $columns['wpseo-linked'] );*/
    $columns = array();
    $columns['id_cleanup'] = "ID";
    $columns['organisateur'] = "Organisateur";
    $columns['type_structure'] = "Type";
    $columns['structure_name'] = "Nom Organisation";
    $columns['name'] = "Nom du CyberCleanUp";
    $columns['date_cleanup'] = "Date";
    $columns['departement'] = "Département";
    $columns['participants'] = "Participants";
    $columns['update'] = "";
    //$columns['publisher'] = __( 'Publisher', 'your_text_domain' );

    return $columns;
}

add_filter( 'manage_edit-cleanup_sortable_columns', 'ihag_manage_column_cleanup',999 );
function my_set_sortable_columns( $columns )
{   
    $columns['id_cleanup'] = "ID";
    unset($columns['organisateur'] );
    $columns['type_structure'] = "type_structure";
    $columns['structure_name'] = "structure_name";
    $columns['name'] = "name";
    $columns['date_cleanup'] = "date_cleanup";
    $columns['departement'] = "departement";
    return $columns;
}

// Administration: Teach wordpress to make the column sortable
function anco_project_year_column_orderby( $vars ) {
    if(isset($_GET['post_type']) && $_GET['post_type'] == "cleanup"){
        if ( isset( $vars['orderby'] ) && 'Type' == $vars['orderby'] ) {
                $vars = array_merge( $vars, array(
                        'meta_key' => 'type_structure',
                        'orderby' => 'meta_value'
                ) );
        } else if ( isset( $vars['orderby'] ) && 'Département' == $vars['orderby'] ) {
                $vars = array_merge( $vars, array(
                        'meta_key' => 'cleanup_departement',
                        'orderby' => 'meta_value_num'
                ) );
        } else if ( isset( $vars['orderby'] ) && 'Nom Organisation' == $vars['orderby'] ) {
            $vars = array_merge( $vars, array(
                    'meta_key' => 'structure_name',
                    'orderby' => 'meta_value'
            ) );
        } else if ( isset( $vars['orderby'] ) && 'Date' == $vars['orderby'] ) {
            $vars = array_merge( $vars, array(
                    'meta_key' => 'date_start',
                    'orderby' => 'meta_value'
            ) );
        } else if ( isset( $vars['orderby'] ) && 'Nom du CyberCleanUp' == $vars['orderby'] ) {
            $vars['orderby'] = 'title';
        }

    }
    return $vars;
}
add_filter( 'request', 'anco_project_year_column_orderby' );

// Add the data to the custom columns for the book post type:
add_action( 'manage_cleanup_posts_custom_column' , 'ihag_custom_cleanup_column', 10, 2 );
function ihag_custom_cleanup_column( $column, $post_id ) {
    $post = get_post($post_id);
    $author_id = $post->post_author;
    $user = get_userdata( $author_id );

    switch ( $column ) {
        case 'organisateur' :
            echo $user->first_name." ".$user->last_name." (".$author_id.")";
            break;
        case 'id_cleanup' :
            echo $post_id; 
            break;
        case 'type_structure' :
            $whichStructureChecked = get_post_meta($post_id, "type_structure", true);
            if($whichStructureChecked == "citizen"){
                echo "Citoyen";
            }
            elseif($whichStructureChecked == "association"){
                echo "Association";
            }
            elseif($whichStructureChecked == "school"){
                echo "École";
            }
            elseif($whichStructureChecked == "collectivity"){
                echo "Collectivité";
            }
            elseif($whichStructureChecked == "company"){
                echo "Entreprise";
            }
            break;
        case 'structure_name' :
            echo get_post_meta($post_id,'structure_name', true); 
            break;
        case 'name' :
            the_title(); 
            break;
        case 'date_cleanup' :
            echo "le ".date_i18n('j/m', strtotime(get_post_meta( $post_id, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta( $post_id, "time_start", true )));
            break; 
        case 'departement' :
            $tab_departements = arrayDepartements();
            echo $tab_departements[get_post_meta( $post_id, "cleanup_departement", true )]." (".get_post_meta( $post_id, "cleanup_departement", true ).")";
            break;  
        case 'participants' :
            $participants = (int)get_post_meta( $post_id, 'participants', true );
			$nb_participant = get_post_meta( $post_id, "nb_participant_max", true );
			echo $participants;
			if(!empty($nb_participant)){
			//	echo ' / '.$nb_participant; 
			}
            break;  
        case 'update' :
            $tab_arg = array(
                'id_cleanup' => $post_id,
                'imadmin' => 1,
            );
            echo '<a href="'.esc_url( add_query_arg( $tab_arg, get_permalink( get_field("page_update_cleanup", "option") ) ) ).'">Modifier</a>';
            break;  
    }
}

add_action( 'restrict_manage_posts', 'ihag_admin_posts_filter_restrict_manage_posts' );
function ihag_admin_posts_filter_restrict_manage_posts(){
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    //only add filter to post type you want
    if ('cleanup' == $type){
        $values = arrayDepartements();
        ?>
        <select name="cleanup_departement">
        <option value="">Filtrer par département</option>
        <?php
            $current_v = isset($_GET['cleanup_departement'])? $_GET['cleanup_departement']:'';
            foreach ($values as $label => $value) {
                printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $label,
                        $label == $current_v? ' selected="selected"':'',
                        $value
                    );
                }
        ?>
        </select>
        <?php
        $whichStructureChecked = array();
        $whichStructureChecked["citizen"] = "Citoyen";
        $whichStructureChecked["association"] = "Association";
        $whichStructureChecked["school"] = "École";
        $whichStructureChecked["collectivity"] = "Collectivité";
        $whichStructureChecked["company"] = "Entreprise";
        ?>
        <select name="type_structure">
        <option value="">Filtrer par Type de structure</option>
        <?php
            $current_v = isset($_GET['type_structure'])? $_GET['type_structure']:'';
            foreach ($whichStructureChecked as $label => $value) {
                printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $label,
                        $label == $current_v? ' selected="selected"':'',
                        $value
                    );
                }
        ?>
        </select>
        <?php
    }
}


add_filter( 'parse_query', 'wpse45436_posts_filter' );
function wpse45436_posts_filter( $query ){
    global $pagenow;
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }
    $meta_query = array();
    if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['cleanup_departement']) && $_GET['cleanup_departement'] != '') {
        $meta_query[] = array(
            'key' => 'cleanup_departement',
            'value' => sanitize_text_field($_GET['cleanup_departement']),
        );
    }
    if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['type_structure']) && $_GET['type_structure'] != '') {
        $meta_query[] = array(
            'key' => 'type_structure',
            'value' => sanitize_text_field($_GET['type_structure']),
        );
    }
    if(count($meta_query) > 1) {
        $meta_query['relation'] = 'AND';
    }
    $query->query_vars['meta_query'] = $meta_query;
}
add_filter('months_dropdown_results', '__return_empty_array');

// Add custom text/textarea attachment field
function add_custom_text_field_to_attachment_fields_to_edit( $form_fields, $post ) {
    $text_field = get_post_meta($post->ID, 'text_field', true);
    $form_fields['text_field'] = array(
        'label' => 'Custom text field',
        'input' => 'text', // you may alos use 'textarea' field
        'value' => $text_field,
        'helps' => 'This is help text'
    );
    return $form_fields;
}
add_filter('attachment_fields_to_edit', 'add_custom_text_field_to_attachment_fields_to_edit', null, 2); 
 
// Save custom text/textarea attachment field
function save_custom_text_attachment_field($post, $attachment) {  
    if( isset($attachment['text_field']) ){  
        update_post_meta($post['ID'], 'text_field', sanitize_text_field( $attachment['text_field'] ) );  
    }else{
         delete_post_meta($post['ID'], 'text_field' );
    }
    return $post;  
}
add_filter('attachment_fields_to_save', 'save_custom_text_attachment_field', null, 2);
 
 
// Add custom checkbox attachment field
function add_custom_checkbox_field_to_attachment_fields_to_edit( $form_fields, $post ) {
    $checkbox_field = (bool) get_post_meta($post->ID, 'child', true);
    $form_fields['child'] = array(
        'label' => 'Checkbox',
        'input' => 'html',
        'html' => '<input type="checkbox" id="attachments-'.$post->ID.'-child" name="attachments['.$post->ID.'][child]" value="1"'.($checkbox_field ? ' checked="checked"' : '').' /> ',
        'value' => $checkbox_field,
        'helps' => ''
    );
    return $form_fields;
}
add_filter('attachment_fields_to_edit', 'add_custom_checkbox_field_to_attachment_fields_to_edit', null, 2); 
 
// Save custom checkbox attachment field
function save_custom_checkbox_attachment_field($post, $attachment) {  
    if( isset($attachment['child']) ){  
        update_post_meta($post['ID'], 'child', sanitize_text_field( $attachment['child'] ) );  
    }else{
         delete_post_meta($post['ID'], 'child' );
    }
    return $post;  
}
add_filter('attachment_fields_to_save', 'save_custom_checkbox_attachment_field', null, 2);


add_filter('acf/settings/remove_wp_meta_box', '__return_false');


function map_ambassadeur(){
    ?>
    <div class="wrap">
        <h1>Carte des ambassadeurs</h1>

        <?php
        $a = array();
        $a['role'] = 'ambassadeur';
        $users = get_users($a);
        $tab_ambassadeur = array();
        foreach($users as $user){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://api-adresse.data.gouv.fr/search/?q=".str_replace(" ", "+", get_user_meta($user->ID, 'user_adress', true).' '.get_user_meta($user->ID, 'user_code_postal', true).' '.get_user_meta($user->ID, 'user_city', true)));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);	

            $r = json_decode( curl_exec($ch) );
            if(!empty($r->features[0]->geometry->coordinates)){
            
                $tab_ambassadeur[] = array(
                    'name'		=> $user->first_name." ".$user->last_name()." - ".get_user_meta($user->ID, 'structure_name', true),
                    'link'		=> admin_url().'user-edit.php?user_id='.$user->ID,
                    'coordonate'=> $r->features[0]->geometry->coordinates[1].','.$r->features[0]->geometry->coordinates[0],
                );
            }
        }
        ?>
        <div id="map_ambassadeur"></div> 
        <style type="text/css">
            #map_ambassadeur{ /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
                height:650px;
            }
        </style>
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
        <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
        <!--<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />-->
        
        <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
        <!--<script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js'></script>-->
        
        <script>
            var tab_ambassadeur = <?php echo json_encode($tab_ambassadeur);?>;
            document.addEventListener('DOMContentLoaded', function() {
                var lat = 46.5;
                var lon = 2.349903;
                var macarte = null;
                var zoom = 6;
                elemMap = "map_ambassadeur";
                //var markerClusters;
                macarte = L.map(elemMap).setView([lat, lon], zoom);
                //markerClusters = L.markerClusterGroup();
                // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
                L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                    // Il est toujours bien de laisser le lien vers la source des données
                    attribution: 'données © OpenStreetMap/ODbL - rendu OSM France',
                    minZoom: 1,
                    maxZoom: 20
                }).addTo(macarte);
                // Nous parcourons la liste des villes

                for (ville in tab_ambassadeur) {

                    coordinates = tab_ambassadeur[ville].coordonate.split(",");

                    lat = parseFloat(coordinates[0]);
                    lon = parseFloat(coordinates[1]);

                    var marker = L.marker([lat, lon]).addTo(macarte);
                    marker.bindPopup('<a href="' + tab_ambassadeur[ville].link + '">' + tab_ambassadeur[ville].name + '</a>');
                    //markerClusters.addLayer(marker);
                }
            });
        </script>
        
    </div>
    <?php
}